+++
author = "+SiritasS"
comments = true
date = "2016-09-21T09:38:33+07:00"
draft = true
image = ""
menu = ""
share = true
slug = "vscode_settings_for_front-end_dev"
tags = ["development"]
title = "เซตอัพ VSCode สำหรับ Front-end dev"

+++

>ทำไมต้องสนใจค่า config ของ text/source code editor ที่ใช้งานด้วย? 

การทำงานร่วมกันเป็นทีม เพื่อให้งานเป็นไปอย่างมีประสิทธิภาพ เราจะต้องลดความสูญเสียต่างๆ 
เช่นตัวอย่างที่พบประจำ คือการแก้ไข format ของ source code เวลาจะเปรียบเทียบ source code หาก developer ใช้ format spec ไม่เหมือนกัน เวลาเอา code มาเปรียบเทียบเพื่อ merge หรืออะไรก็แล้วแต่ จะเสียเวลามาก

การกำหนดเครื่องมือให้คนในทีมใช้เหมือนกันก็เป็นข้อหนึ่งที่ควรทำ เมื่อใช้เครื่องมือตัวเดียวกันแล้ว config ของเครื่องมือก็ต้องเหมือนกันด้วย 

###ค่า Config ที่ใช้ใน VSCode สำหรับ Angular2 

**EditorConfig** 

ตัวนี้ต้องลง plugin ชื่อเดียวกันนี้เพิ่มด้วย ค่า config ข้างล่างนี้รวบรวมมาจากโครงการ opensources ใหญ่ๆ เช่น AngularJS, Bootstrap

file: `.editorconfig`

```
# http://editorconfig.org
root = true

[*]
indent_style = space
indent_size = 2
charset = utf-8
trim_trailing_whitespace = true
insert_final_newline = true
end_of_line = lf

[*.md]
trim_trailing_whitespace = false

[*.sass]
indent_style = space
indent_size = 2

[*.json]
indent_style = space
indent_size = 2

[*.js]
indent_style = space
indent_size = 2
```

**Typescript 1.8 or 2.0**

ตอนนี้ใช้ VSCode v1.5.2 กับ Angular2 final released ซึ่งใช้ Typescript 2.0.2
ทุกครั้งที่เปิด VSCode ขึ้นมาใช้งานมันจะต้องถามว่าจะใช้ 1.8 หรือ 2.0 

แก้ไขได้โดยกำหนดค่า config แบบนี้ ให้มันหา typescript lib จาก node_modules ใน project ของเราเลย 

file: `.vscode/settings.json` หรือจะกำหนดที่ User Settings [`Command+,`] ก็ได้

```
// Place your settings in this file to overwrite default and user settings.
{
  "typescript.tsdk": "node_modules/typescript/lib"
}
``` 