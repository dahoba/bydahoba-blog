+++
author = "ssiritas"
comments = true
date = "2016-03-16T16:12:13+07:00"
draft = false
image = ""
menu = ""
share = true
slug = "common-instruction-to-put-SSH-key-to-favorite-git-hosting"
tags = ["development", "tips"]
title = "สรุปวิธี สร้างและเพิ่ม SSH key ไปใส่ใน git hosting Github, Gitlab, Bitbucket"

+++

<img src="https://i.imgur.com/7oiK4Ev.jpg" width="640" />

ตอนนี้มี Git hosting มาให้เลือกใช้มากมาย ที่งานใช้งานบ่อยๆ ช่วงนี้ก็มี

* [Github](https://github.com/)
* [Bitbucket](https://bitbucket.org)
* [Gitlab](https://gitlab.com/)

2 ตัวหลังนี่ใช้บ่อยมาก เพราะให้เราสร้าง private project ได้ ไม่จำกัด (ไปจำกัดเรื่องอื่นแทน เช่น จำนวน user/repo, ขนาดของ repo)

สำหรับการใช้งาน git repository ทุกที่ข้างต้น เราจะต้องมี ssh key แลกกับ server เค้า **บันทึกนี้จะเป็นขั้นตอนที่ผมทำเวลาสร้าง ssh key** ซึ่งปกติเค้าก็จะทำกันครั้งเดียวแหละ เว้นเสียแต่ว่าเราจะทำ ssh key หาย ซึ่งจะทำให้เรา push source code ขึ้นไปไม่ได้

<!--more-->

**ขั้นตอนในภาพรวม**

* ตรวจสอบว่าเรามี SSH client หรือยัง
* สร้าง SSH key สำหรับ git hosting แต่ละเจ้าแยกกัน
* สร้าง key แล้ว เพิ่ม key เข้าไปใน ssh-agent
* นำ public key (ไฟล์ .pub) ไปใส่ใน git hosting

**ตรวจสอบ SSH client**

เปิด Terminal ใช้คำสั่ง (สำหรับ Windows ใช้ Git bash)

```
$ ssh -V
```
ถ้ามี เราจะได้ข้อมูล version ของ SSH client ที่ติดตั้งอยู่ คล้ายๆ แบบนี้

_อันนี้ของ OSX_

```
OpenSSH_6.9p1, LibreSSL 2.1.8
```

**สร้าง SSH key**

หากไปดูเอกสาร help ของแต่ละที่เขาจะให้เราหาว่าเครื่องเรามี ssh key อยู่แล้วหรือยัง แต่สำหรับผม ผมชอบที่จะสร้าง key แยกกัน โดยเปลี่ยน email address ที่ลงทะเบียนกับ git hosting แตกต่างกัน

คุณรู้หรือไม่ว่า gmail สามารถให้เราทำ label email address ได้ เพียงแค่เพิ่ม +label เข้าไป

ตัวอย่าง: `myemail+github@gmail.com` หรือ  `myemail+gitlab@gmail.com`

สำหรับ email service เจ้าอื่นผมไม่แน่ใจเหมือนกัน แต่ก็ใช้วิธีสร้าง ssh key แบบเดียวกันนี่แหละครับ

มาเริ่มสร้าง ssh key กัน เปิด Terminal ใช้คำสั่งต่อไปนี้

```
$ cd ~/.ssh
```

สำหรับ window น่าจะอยู่ที่ path นี้ `cd %userprofile%\.ssh`

```
$ ssh-keygen -t rsa -b 2048 -C "your_email+github@gmail.com"
```

`-C` option นี้คือ comment เราจะใส่ email address อันที่ลงทะเบียนกับ git hosting เจ้านั้นๆ
`-b` จำนวน bits ที่จะใช้ในการสร้าง key สำหรับแบบ rsa ใช้ 2048 ถือว่าเพียงพอแล้ว
`-t` เราจะใช้ key encrytion แบบ rsa

กด Enter แล้วจะเจอคำถาม

```
Enter a file in which to save the key (/Users/you/.ssh/id_rsa): [Press enter]
```
ให้เปลี่ยนชื่อไฟล์ก่อนจะกด Enter เช่น ผมจะเปลี่ยนเป็น `id_rsa_github` สำหรับสร้าง key ของ github

ต่อไป แนะนำให้ใส่ passphrase หรือ password สำหรับ key ที่กำลังจะสร้างด้วย ถือเป็นอีกด่านนึง หาก key หลุดออกไปสู่สาธารณะ จะต้องรู้ password ด้วย ถ้าไม่อยากใส่ passpharse ก็ Enter ข้ามไปเฉยๆ

```
Enter passphrase (empty for no passphrase): [Type a passphrase]
Enter same passphrase again: [Type passphrase again]
```

หลังจากใส่ passpharse แล้ว เราจะได้ผลคล้ายแบบนี้

```
Your identification has been saved in id_rsa_github.
Your public key has been saved in id_rsa_github.pub.
The key fingerprint is:
SHA256:****7Ts7ALeuST60fODcLweRT3N1gqtYN8UaLri1eh4 d***+github@gmail.com
The key's randomart image is:
+---[RSA 2048]----+
|             o   |
|         . .o + o|
|        ...o.= o.|
|       oo B.O .  |
|        SB X.+   |
|       oB.*.oo   |
|        +*.+o.   |
+----[SHA256]-----+
```

อันนี้ถือว่าเสร็จการสร้าง key สำหรับ git hosting 1 เจ้า สำหรับเจ้าต่อๆไป ผมก็จะเปลี่ยน comment `-C` ตรง command line ให้ตรงกับ email ที่ใช้ register กับ hosting นั้นๆ

**เพิ่ม key เข้าไปใน ssh-agent**

ssh-agent เป็นเครื่องมือช่วยเราจำ passphrase ของ ssh key ที่เราสร้างและใช้งาน ถ้าเราไม่มี passphrase ก็อาจจะข้ามขั้นตอนนี้ไปได้

ใช้ Terminal เหมือนเดิม ใช้คำสั่งนี้

```
$ eval "$(ssh-agent -s)"
```
เพื่อเปิดใช้งาน ssh-agent

แล้วใช้คำสั่งนี้เพื่อเพิ่ม key เข้าระบบ

```
$ ssh-add ~/.ssh/id_rsa_github
```
จำนวนครั้งที่ต้องรันก็ต่างกันไปตามจำนวน key ที่สร้างออกมานะครับ อย่างผมก็ต้องทำ 3 ครั้ง แบบนี้

```
$ ssh-add ~/.ssh/id_rsa_github
$ ssh-add ~/.ssh/id_rsa_gitlab
$ ssh-add ~/.ssh/id_rsa_bitbucket
```
ทดสอบว่าเพิ่ม key สำเร็จหรือไม่ ใช้คำสั่ง

```
$ ssh-add -l
```
 จะเห็นผลประมาณนี้

`2048 SHA256:**Ts7ALeuST60fODcLweRT3N1gqtYN8UaLri**** id_rsa_github (RSA)`

**เอา public key ไปใส่ที่ git hosting**

ใช้คำสั่ง

```
$ cat ~/.ssh/id_rsa_github
```
ก็จะได้ text ออกมาประมาณนี้

```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCtVoFJnnKY2YKGAqCKY9S97JUmw5Taj5nr
...
TBzs0C0HmUB53mqI/J9q2fmEdBEOMNYbw== d***+github@gmail.com
```
ให้ copy ข้อความ (เค้าเรียก public key) เอาตั้งแต่ ssh-rsa จนไปจบ @gmail.com ไปใส่ที่หน้า SSH key ของแต่ละ hosting ที่ตรงกับ email ของ key นั้นๆ

เช่นอันนี้ของ github
![github add ssh key](http://i.imgur.com/wILlzf7.png)

ใส่แล้วจะเป็นแบบนี้

![github show key](http://i.imgur.com/aUatJys.png?1)

สรุปจาก:
[bitbucket help](https://confluence.atlassian.com/bitbucket/how-to-install-a-public-key-on-your-bitbucket-cloud-account-276628835.html), [gitlab help](https://gitlab.com/help/ssh/README), [github help](https://help.github.com/articles/generating-an-ssh-key/)