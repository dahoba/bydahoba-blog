+++
author = "+SiritasS"
comments = true
date = "2016-10-07T09:47:16+07:00"
draft = false
image = ""
menu = ""
share = true
slug = "diy-mac-sierra-iso-and-install-in-virtualbox"
tags = ["osx", "tips"]
title = "ทำ macOS Sierra's iso จาก .app และติดตั้งลง Virtualbox"

+++

เมื่อสองสามสัปดาห์ก่อน Apple ออก OS version ใหม่สำหรับเครื่อง Mac, Macbook ปกติผมจะรีบตามอัพเดตตลอด
แต่ตอนนี้ไม่รู้ทำไม ไม่มีแรงจูงใจมากพอที่จะอัพเดต OS ของ macbook ที่ตัวเองใช้ทำงานอยู่เลย

อืม...แต่ก็นึกถึง blog [วิธีติดตั้ง NTFS ใน El Capitan](http://www.dahoba.xyz/2015/10/os-x-os-x-1011-el-capitan-ntfs.html) เพื่อให้เครื่อง Mac
อ่าน+เขียน disk ที่ใช้ใน windows ได้ เราจะต้องรีบเพิ่มเนื้อหาสำหรับ mac OS Sierra ซะหน่อย
คิดๆว่าทำไงถึงจะเขียนได้โดยยังไม่ต้องอัพเดต OS ในเครื่องตัวเอง เลยได้มาเป็นโพสนี้

<!--more-->

ผมจะติดตั้ง macOS Sierra ลงใน Virtualbox เพื่อใช้ทดสอบ หรือทดลองการติดตั้ง NTFS driver ถ้าลองไป Google ดู
ก็จะเจอวิธีจากหลายที่เลย ส่วนใหญ่ก็จะทำเป็น iso มาให้แล้ว และให้ download เอาไปใช้กับ virtualbox เลย (ส่วนมากก็เอาไปทำ Hackintosh; รัน macOS บนเครื่องที่ลง Window เอาไว้)

### TL;DR ยาวไปไม่อ่าน
>
>เป็นการลง/รัน macOS ใน virtual machine ที่รันอยู่บน macOS เหมือนกัน และก็จะสร้าง iso image เองด้วย ไม่ใช่ให้ download pre-made image
>
หาๆ หลายชั่วโมง ทดลองมาหลายที่ ถ้าไม่อยากเสียเวลาก็ทำตามโพสนี้ได้เลย ทำมาแล้ว worked! ;)
>
scroll ไปดูขั้นตอนได้เลยหากไม่อยากอ่านที่มาที่ไป :P
>

### วิธี/ขั้นตอน

1. ต้องเตรียม ตัวติดตั้ง macOS Sierra ก่อน ไฟล์มันจะชื่อว่า `Install macOS Sierra.app` ดาวโหลดได้จาก App Store หรือจะจากแหล่งอื่นก็ได้นะ แต่เอาที่ไม่ได้ modify อะไรเลย
2. ติดตั้ง VirtualBox 5.x เอาไว้ก่อน อย่างผมใช้ 5.0.x ตัวอย่างใน script ข้อต่อไปเค้าใช้ 5.1.x ใช้ได้เหมือนกัน
3. ให้ดาวโหลด script จาก [github นี้](https://github.com/geerlingguy/macos-virtualbox-vm/archive/master.zip)
4. เมื่อแตก zip ออกมาจะมีไฟล์ `prepare-iso.sh` ให้ทำให้มัน execute ได้

```
 $ chmod +x prepare-iso.sh
```
5 สั่งให้มันทำงาน

```
$ ./prepare-iso.sh
```
โดย script จะไปหา `Install macOS Sierra.app` ที่อยู่ใน `/Applications` ถ้าเราเก็บไว้ที่ path อื่น ก็ต้องใส่ absolute path เข้าไปเป็น argument แรก

*ตัวอย่าง*

ผมเก็บเอาไว้ใน path `/Users/siritas/tmp` ก็ใช้แบบนี้ แล้วรอมันทำงานสักครู่ จะได้ไฟล์ Sierra.iso วางเอาไว้ให้ที่ Desktop

```
./prepare-iso.sh /Users/siritas/tmp/Install\ macOS\ Sierra.app
```

6 สร้าง Virtual machine ตัวใหม่ โดยมีจุดสำคัญดังนี้

* เลือก System OS เป็น **OSX 64bit**
* เลือก System Chipset เป็น **PIIX3**
* Base memory **4096 (4GB)** เป็นอย่างน้อย
* Boot Order Optical, Harddisk -- uncheck Floppy ออก
* Display > Video memory ให้ใช้ **128MB**
* Uncheck `Enable Audio` ปิด sound card ซะ

<img src="https://i.imgur.com/gbMUCLll.png" title="source: imgur.com" />
<img src="https://i.imgur.com/7BRmksCl.png" title="source: imgur.com" />
<img src="https://i.imgur.com/75ZoMs8l.png" title="source: imgur.com" />
<img src="https://i.imgur.com/JQpBsGtl.png" title="source: imgur.com" />

กด Next ต่อๆไป จนเสร็จก่อนจะกด Start ให้

7 พิมพ์คำสั่งเพื่อเซตให้ macOS มันยอม boot ใน virtualbox

```
 $VBoxManage modifyvm "{vmname}" --cpuidset 00000001 000306a9 00020800 80000201 178bfbff
```

แทนที่ `{vmname}` ด้วยชื่อของ vm ที่ตั้งชื่อไว้

8 นำ iso ในขั้นตอนแรกมาใส่ลงไปช่อง cdrom ของ vm

<img src="https://i.imgur.com/eqXBqhi.png" title="source: imgur.com" />

9 กด Start VM ขั้นตอนที่เหลือก็เหมือนตอนเราซื้อเครื่องมาใหม่ใช้งานครั้งแรก หรือตอนติดตั้ง OS ใหม่นั้นเอง

<img src="https://i.imgur.com/GtfiDdEl.png" title="source: imgur.com" />
<img src="https://i.imgur.com/R3dwYsal.png" title="source: imgur.com" />

**แหล่งศึกษา | Resources**

* [Instructions and script to help you create a VirtualBox VM running macOS](https://github.com/geerlingguy/macos-virtualbox-vm)
* [How to install Mac OS X Sierra 10.12 on VirtualBox](http://hintdesk.com/how-to-install-mac-os-x-sierra-10-12-on-virtualbox/ )


