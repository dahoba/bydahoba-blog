+++
author = "siritas"
comments = true
date = "2016-06-10T09:27:46+07:00"
draft = false
image = ""
menu = ""
share = true
slug = "top-promises-pattern-handling-articles"
tags = ["development"]
title = "รวมบทความเด่น จาวาสคริปต์โพรมิส และการรับมือกับ Error : Promises & Error handling Best Practices"

+++

![Imgur](https://i.imgur.com/3Noo4wzl.jpg)

ช่วง 2-3 เดือนนี้ได้เขียนจาวาสคริปต์จริงจังหน่อย (ทุกค่ำวันทำงานเลย) ลองเขียนแอพด้วย ionic framework 2 ซึ่งเป็น Angular 2 ด้วย พอใส่ความสามารถของแอพมากๆเข้า ก็ใช้ Promises เยอะขึ้นเรื่อยๆ เริ่มเกิด Promises hell คือโค้ดมันดูไม่ได้เลย -_-" ไม่รู้จะจัดการความยุ่งเหยิงนี้อย่างไร จึงไปค้นคว้าๆ

เหล่าเซียนจาวาสคริปต์ได้บันทึก Best practices, Promises Pattern เอาไว้พอสมควร รวบรวมไว้ดังนี้
<!--more-->

* [JavaScript Promises and Error Handling](http://odetocode.com/blogs/scott/archive/2015/10/01/javascript-promises-and-error-handling.aspx) --- เขียน Promises แล้วจะจัดการกับ error ที่เกิดขึ้นใน chain ได้อย่างไร (chain เกิดขึ้นเมื่อเราเรียก promises ต่อๆกันเป็นทอดๆ)   
* [JavaScript Promises - There and back again](http://www.html5rocks.com/en/tutorials/es6/promises/) --- อันนี้ละเอียดเลย บอกที่มาที่ไปของ Promises เขียนยังไงให้ดี   
* [We have a problem with promises](https://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html) --- รวบรวมปัญหาที่นักพัฒนามักจะเจอกับ Promises พร้อมแนะนำวิธีป้องกัน และแก้ปัญหา

**ด้านล่างเน้นที่ Pattern, Anti-Pattern**  

Pattern คือสิ่งที่เค้าคิดกันมาแล้วว่าดี เป็นระเบียบ ทำตามได้เลย  Anti-pattern ก็คือแบบที่ทำสวนทางกับ Pattern

* [5 Promises pattern](https://remysharp.com/2014/11/19/my-five-promise-patterns)
* [Best Practices for Using Promises in JS](https://60devs.com/best-practices-for-using-promises-in-js.html)
* [Promise Patterns & Anti-Patterns](http://www.datchley.name/promise-patterns-anti-patterns/)

ส่งท้าย ผมเขียนๆไปก็จะ refactoring ไปด้วย ปรับปรุงโค้ดให้ดียิ่งๆขึ้น ทำตาม style guide ของ Angular จริงๆก็คล้ายกับทุกๆ framework ทุกๆภาษาที่เขียน คือเช่น

* พวก method ต้องมีหน้าที่เดียวหรือมีหน้าที่น้อยที่สุด (single responsibility)
* จำนวนบรรทัดของซอร์สโค้ดในหนึ่งเมททอต
* การตั้งชื่อ component, service
* ฯลฯ [Angular 2 Style guide](https://angular.io/docs/ts/latest/guide/style-guide.html)

