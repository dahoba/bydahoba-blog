+++
author = "+SiritasS"
comments = true
date = "2016-10-26T11:33:59+07:00"
draft = false
image = ""
menu = ""
share = true
slug = "open-source-booking-system"
tags = ["open-source", "development"]
title = "ระบบจองแบบ Open Source"

+++

ช่วงนี้ได้ยิน/ได้เกี่ยวข้องกับระบบการจองต่างๆบ่อย เลยนึกขึ้นมาว่าลองค้นหาดูหน่อยซิ
ว่ามีคน open source ระบบการจองนู่นจองนี้เอาไว้บ้างรึเปล่า

<!--more-->

ค้นอยู่ 10-20 นาทีได้มาประมาณนี้ แปะเอาไว้ก่อน

[Classroombookings](http://classroombookings.com/) -- Open source hassle-free room booking system for schools.

[Booked](https://sourceforge.net/projects/phpscheduleit/) -- Web-based calendar and schedule (formerly phpScheduleIt).

[Meeting Room Booking System](https://sourceforge.net/projects/mrbs/) -- MRBS is a system for multi-site booking of meeting rooms.

[Open Camp Ground](http://www.opencampground.com/) -- Open Campground is a free open source campground reservation system.

[Alf.io/](https://alf.io/) -- The open source ticket reservation system.
