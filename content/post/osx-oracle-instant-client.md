+++
author = "daho"
comments = true
date = "2016-06-24T16:33:56+07:00"
draft = false
image = ""
menu = ""
share = true
slug = "install-oracle-instant-client-on-osx10"
tags = ["osx", "development"]
title = "[OSX] ติดตั้ง sqlplus (Oracle instant client) ใน Mac"

+++
<img src="https://i.imgur.com/FPHc34hl.png" width="640" height="320" />
<!--more-->
ดาวโหลดไฟล์ที่ใช้ได้จาก [ลิ้ง](http://www.oracle.com/technetwork/topics/intel-macsoft-096467.html) นี้

ถ้าใช้ OSX OS X El Capitan, Yosemite and Mavericks โหลด 12.1.x.x นะ เก่ากว่านั้นอาจจะใช้งานไม่ได้

สมัครสมาชิก Oracle OTN ให้เรียบร้อย แล้ว กดเลือก Accept License

![Accept License](https://i.imgur.com/mm8FaiB.png)

เราจะใช้ sqlplus จำเป็นต้องโหลด 2 ตัวนี้

* Instant Client Package - Basic Lite: Smaller version of the Basic, with only English error messages and Unicode, ASCII, and Western European character set support
`Instantclient-basiclite-macos.x64-12.1.0.2.0.zip`
* Instant Client Package - SQL*Plus: Additional libraries and executable for running SQL*Plus with Instant Client
 `instantclient-sqlplus-macos.x64-12.1.0.2.0.zip`

เปิด Terminal

สร้าง folder กันก่อน

```
$ mkdir ~/Oracle
```

แตก zip 2 ไฟล์นี้ที่ดาวโหลดมาได้

```
$ unzip instantclient-basiclite-macos.x64-12.1.0.2.0.zip -d ~/Oracle
$ unzip instantclient-sqlplus-macos.x64-12.1.0.2.0.zip -d ~/Oracle
```

เปลี่ยน folder ไปที่เราเพิ่งแตก zip

```
$ cd ~/Oracle/instantclient_12_1
```

เราจะสร้าง symbolic link ให้กับ library file ตามที่ Oracle กำหนดมา

```
$ ln -s libclntsh.dylib.12.1 libclntsh.dylib
```
จำเป็นต้องใช้ OCCI lib ด้วย พิมพ์ต่อ

```
$ ln -s libocci.dylib.12.1 libocci.dylib
```

ต่อไปจะต้องเพิ่ม path เพื่อให้เราสามารถเรียกคำสั่งใน folder นี้ได้

ตรวจสอบก่อนว่ามีอะไรอยู่ในตัวแปร PATH

```
$ echo $PATH
/usr/local/sbin:/opt/subversion/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin
```
ต่อไป คือคำสั่งเพิ่ม path ที่จะเรียก program โดยเอา path ที่มีอยู่เดิมมาต่อท้าย path ใหม่

```
$ export PATH=/Users/{siritas_s}/Oracle/instantclient_12_1:$PATH
```

ทดสอบเรียก ... เห็นแบบข้างล่าง แสดงว่าใช้ได้แล้ว

```
$ sqlplus

SQL*Plus: Release 12.1.0.2.0 Production on Fri Jun 24 16:13:38 2016

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

Enter user-name:
```

[วิธีเป็นภาษาอังกฤษ](http://www.oracle.com/technetwork/topics/intel-macsoft-096467.html#ic_osx_inst)
