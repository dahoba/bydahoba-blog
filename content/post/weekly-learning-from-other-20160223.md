+++
author = "siritas"
comments = true
date = "2016-02-23T10:43:06+07:00"
draft = false
share = true
slug = "weekly-learning-from-other-20160223."
tags = ["research"]
title = "Weekly learning from other 20160223"

+++

After study these blog resources, I might blog about what I'd learned from them.
<!--more-->

### Interesting stories from others this week

* [Keeping Control of the Front-end](http://blog.booking.com/keeping-control-of-the-front-end.html?ref=slicedham)
* [Sending emails to our half million and growing user community](http://engineering.hackerearth.com/2016/02/11/sending-emails-to-our-half-million-and-growing-user-community/?ref=slicedham)
* [Developing Eventbrite Using Docker](http://www.eventbrite.com/engineering/developing-eventbrite-using-docker/?ref=slicedham)
* [Feature Toggles](http://martinfowler.com/articles/feature-toggles.html#WorkingWithFeature-toggledSystems?ref=slicedham)
* [Are You Still Debugging?](http://www.yegor256.com/2016/02/09/are-you-still-debugging.html?ref=slicedham)
* [Getting Started with ES6 (ES2015) and Babel](https://davidwalsh.name/es2015-babel?ref=slicedham)
* [What Is AMP?](https://www.ampproject.org/docs/get_started/about-amp.html)
* [Issues Pull Request Template](https://github.com/blog/2111-issue-and-pull-request-templates)