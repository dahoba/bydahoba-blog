+++
author = "dahoba"
comments = true
date = "2016-04-22T11:20:49+07:00"
draft = false
image = ""
menu = ""
share = true
slug = "upgrade-subversion-on-ubuntu-1204-the-safeway"
tags = ["ubuntu", "tips"]
title = "Ubuntu: วิธีปลอดภัย ในการเปลี่ยน subversion เป็น version 1.7 ขึ้นไป ในอูบุนตู 12.04 LST | Upgrade subversion (to 1.7+) on Ubuntu 12.04 the safe way"

+++

ถ้าเรา google หาวิธีอัพเกรด version ของ subversion จะเจอเยอะแยะ ส่วนนึงก็จะให้ไปเพิ่ม ppa ของ subversion เลย บ้างก็ให้
เพิ่ม repository โดยชี้ไปที่ URL ของตนเอง แต่วิธีที่จะนำเสนอนี้ดูแล้วว่ามันปลอดภัยระดับนึง ผมเลือกใช้
repository ของ Wandisco ซึ่งเค้าทำธุรกิจที่ต้องใช้ subeversion ทำมาหากิน เชื่อใจได้ระดับนึงว่า
ของเค้าต้อง stable และมีการ maintenance ด้วย

![Imgur](https://i.imgur.com/NqvyIVvm.png)

<!--more-->

**วิธีก็ทำตามนี้**

```sh
echo "deb http://opensource.wandisco.com/ubuntu precise svn17" | sudo tee /etc/apt/sources.list.d/svn.list
sudo wget -q http://opensource.wandisco.com/wandisco-debian.gpg -O- | sudo apt-key add -
sudo apt-get update
sudo apt-get install subversion
```

อธิบายคำสั่ง

* บรรทัดที่ 1 เพิ่ม wandisco repository ลงใน sources.list ตรงนี้่ให้สังเกต `precise` กับ `svn17`
มันเปลี่ยนได้. Wandisco ทำ respository เอาไว้จนถึง subverion 1.9 เลย จะเปลี่ยน release
ไม่เอา precise ก็ยังได้ ลอง browse URL `http://opensource.wandisco.com/ubuntu` ดูได้

![Imgur](https://i.imgur.com/EiMPpnx.png)

* บรรทัดที่ 2 คือเราต้องเพิ่ม PGP key ของ wandisco เอาไว้ด้วย ไม่ทำไม่ได้
* บรรทัดที่ 3 สั่งให้ ubuntu update รายการของ package และเวอร์ชั่นใหม่ๆ
* บรรทัดที่ 4 สั่งติดตั้ง subversion อีกครั้ง ถ้ามันเจอเวอร์ชั่นใหม่กว่ามันก็จะติดตั้งตัวที่ใหม่ที่สุดให้

 ตัวอย่าง console หลังเรียกคำสั่ง
```sh
$ sudo apt-get install subversion
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following package was automatically installed and is no longer required:
  libdb4.8
Use 'apt-get autoremove' to remove them.
The following extra packages will be installed:
  libserf1 libsvn1
Suggested packages:
  subversion-tools db4.8-util
The following NEW packages will be installed:
  libserf1
The following packages will be upgraded:
  libsvn1 subversion
2 upgraded, 1 newly installed, 0 to remove and 143 not upgraded.
Need to get 2687 kB of archives.
After this operation, 3883 kB of additional disk space will be used.
Do you want to continue [Y/n]? Y
```

เมื่อมันติดตั้งเสร็จไม่มีข้อความ error อะไร เราจะทดสอบดูเวอร์ชั่นของ subversion หลังการอัพเกรดดู

```sh
$ svn help

usage: svn <subcommand> [options] [args]
Subversion command-line client, version 1.7.22.
Type 'svn help <subcommand>' for help on a specific subcommand.
Type 'svn --version' to see the program version and RA modules
  or 'svn --version --quiet' to see just the version number.

Most subcommands take file and/or directory arguments, recursing
on the directories.  If no arguments are supplied to such a
command, it recurses on the current directory (inclusive) by default.
```

ได้มาแล้ว version 1.7.22 :)

**แหล่งศึกษา:**

* [HOWTO: Upgrade to Subversion 1.7](http://ubuntuforums.org/showthread.php?t=1876156)
* [Wandisco](http://www.wandisco.com/subversion-products)