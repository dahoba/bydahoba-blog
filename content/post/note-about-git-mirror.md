+++
author = "+SiritasS"
comments = true
date = "2016-10-21T17:48:00+07:00"
draft = true
image = ""
menu = ""
share = true
slug = "note-about-git-mirror"
tags = ["git"]
title = "การ copy Git repo จาก Cloud มาที่ office"

+++

บันทึกเอาไว้ เมื่อเร็วๆนี้ ได้ติดตั้ง Git server ภายใน office เลือกใช้ Gitlab CE เนื่องจากมันมีหน้าจอ web
ให้ใช้สวยๆ มีระบบอื่นๆที่ใช้ข้างเคียงกันให้ใช้ด้วยเช่น issue tracker, wiki และยังเชื่อมต่อกับ ldap หรือ
 active directory ที่ office ใช้งานอยู่ได้ด้วย (ทำให้นักพัฒนาไม่ต้องจำ username password หลายชุด)

การ copy มานี้คือต้องการได้ history จากต้นทางมาด้วย ที่ศึกษาไว้มันทำได้**อย่างน้อย** 2 วิธี ซึ่งอาจจะมีวิธี
มากกว่าที่ผมรู้ก็ได้ ฉนั้นในโพสนี้จะบอกแค่ 2 วิธี

### วิธีแรก

ใน Gitlab เมื่อเราสร้าง project ใหม่ขึ้นมา มันจะมี option ให้เลือก import repository จาก Git hosting
เจ้าดังๆให้อยู่แล้ว เช่น Github Bitbucket วิธีนี้ เราจะต้องแก้ไข configuration ของ Gitlab ที่ server
และต้องมีการ restart server ด้วย

เริ่มจาก ต้นทาง Gitlab ก็ต้อง config ให้รองรับ request พิเศษจาก server ของเรา ไปที่ menu Application
[GitLab as OAuth2 authentication service provider](http://hqrepossvr.softsquaregroup.com:8888/help/integration/oauth_provider.md#gitlab-as-oauth2-authentication-service-provider)

ต่อมาที่ฝั่งserverเรา เราจะต้อง config ตัว OmniAuth ดูเพิ่ม[ที่นี่](http://hqrepossvr.softsquaregroup.com:8888/help/integration/omniauth.md)

เอา application id, secret ที่ได้จากขั้นตอนก่อนหน้ามาใส่

แล้ว restart Gitlab service หนึ่งครั้ง

เข้าหน้าจอสร้าง project เราจะกด import จาก Gitlab.com ได้แล้ว


### วิธีที่สอง

ใช้ command line ล้วนๆ

```
git clone --bare https://github.com/exampleuser/old-repository.git
# Make a bare clone of the repository

cd old-repository.git
git push --mirror https://hqrespossvr/user/new-repository.git
# Mirror-push to the new repository

cd ..
rm -rf old-repository.git
# Remove our temporary local repository
```

from:

1 https://www.smashingmagazine.com/2014/05/moving-git-repository-new-server/
2 https://help.github.com/articles/duplicating-a-repository/

