+++
author = "+SiritasS"
comments = true
date = "2016-03-23T14:27:56+07:00"
draft = false
image = ""
menu = ""
share = true
slug = "hardening-ssh-from-internet"
tags = ["devops", "security"]
title = "ป้องกัน SSH server จากการโจมตี Brute Force | HOWTO Hardening SSH server"

+++
<img src="https://i.imgur.com/7cGTp59.jpg" width="640" />
ทาง network admin อีเมลมาว่า firewall ของบริษัทตรวจจับการโจมตีแบบ brute force มายัง SSH server ที่เปิดให้ login ผ่าน internet ได้  
_โชคดี_ firewall ตรวจจับ เอาไว้ได้ ให้ทางผมทำอะไรหน่อยเพื่อให้มันเจาะไม่ได้ เช่น เปลี่ยน password   
แต่ _โชคร้าย_ server ตัวนึงโดนยึดไปได้ (มันเปลี่ยน root password ...ไอ่ฉิบ~) ดีว่าไม่ใช่ตัวสำคัญ และเป็น VM เลย shutdown แล้วใช้  single mode เข้าไป reset จากนั้นค้นหาทันทีว่าจะป้องกัน SSH จากการโจมตีแบบ Brute force ได้อย่างไรบ้าง 

<!--more-->

ได้ความมาว่า 

- เราต้องไม่ให้ root login ผ่านทาง internet ได้
- ใช้ password ที่ brute force ได้ยาก 
- หรือ ไม่ใช้วิธี login โดยการใส่ password ให้ใช้ private/public key แทน
- และอีกหลายคำแนะนำ ถ้าสนใจตามอ่านได้จาก link ใต้เรื่องแหล่งศึกษา

**สรุปสิ่งที่จะทำ**

1. เปลี่ยน password ของ SSH user ให้ **ยากที่จะ brute force**
2. ใช้ private/public key แทนการใช้ password
2. ปิดการ login ด้วย password 
3. ไม่ให้ root login จาก internet ได้

**ตั้ง password แบบ brute force ได้ยาก**

วิธีตั้ง password ผมใช้ [LastPass password generator](https://lastpass.com/f?1553846) (เป็น extension ใช้งานใน browser ได้ฟรี ช่วยจำพาสเวิร์ด และมีตัวช่วยสร้าง password ด้วย) โดยเซตให้สุ่มความยาว 10 ตัว ให้ออกเสียงได้ (Pronounceable) แล้วผมจะเพิ่ม ตัวเลข 1 ตัว กับสัญลักษณ์พวก `!@#$%^&*-_+=` เลือกมาอีก 1 ตัว แทรกเข้าไปก่อนจะนำไปใช้

เช่น

`LoRImAndeM` ถ้าลองเอาไปทดสอบ จะได้ว่าใช้เวลา 1 เดือนก็แกะได้แล้ว 

เพียงเพิ่ม `=` กับ `3` แทรกเข้าไป แบบนี้

`LoRIm=Ande3M` เอาไปทดสอบอีกครั้ง จะกลายเป็นต้องใช้เวลาถึง 34 พันปี (34 THOUSAND YEARS)
 
เครื่องมือตรวจสอบว่า password ที่ใช้จะต้องใช้เวลานานที่สุดแค่ไหนที่จะ brute force สำเร็จ
อยู่ที่ [how secure is mypassword](https://howsecureismypassword.net)

เมื่อเลือกมาแล้วก็เอาไปเปลี่ยนให้ SSH account ซะ

**ใช้ private/public key** 

มีหลักการให้เชื่อได้ว่าการ brute force key นั้นด้วย technology ตอนนี้ยังไม่สามารถทำได้สำเร็จ ต่อให้ใช้ computer cluster กันเต็มทั่วพื้นที่ในโลกของเรา การคำนวนเพื่อเจาะ key ขนาด 128 bits ต้องใช้เวลาถึง 1000 ปี; ค่า default ของการใช้คำสั่ง ssh-keygen อยู่ที่ 2048 bits

การ generate private/public key ดูได้จากโพสเก่า [สรุปวิธี สร้างและเพิ่ม SSH key ไปใส่ใน git hosting Github, Gitlab, Bitbucket](https://by.dahoba.xyz/post/common-instruction-to-put-SSH-key-to-favorite-git-hosting/)

**ปิดการ login ด้วย password**

แก้ไขไฟล์ `/etc/ssh/sshd_config` หา `#PasswordAuthentication yes` แล้วลบ `#` ออกและเปลี่ยนจาก yes เป็น no ก็คือ `PasswordAuthentication no`  
เมื่อแก้ไข และบันทึกแล้ว restart service sshd 

ถ้าใช้ redhat/centos ใช้คำสั่ง 

`$ /etc/init.d/sshd restart` 

ถ้าเป็น ubuntu 

`$ sudo service ssh restart`

เวลาจะ login ไปที่  server ตัวนั้นๆ จะใช้แบบนี้แทน 

```
$ ssh ubuntu@172.16.0.200 -i ~/.ssh/id_rsa.pub 
```
หรือ วิธีที่สะดวกกว่า ไม่ต้องมานั่งพิมพ์ -i pubkey ทุกครั้ง คือไปแก้ไข `.ssh/config` 

```
  Hostname 172.16.0.200
  Port 22
  User ubuntu
  IdentityFile ~/.ssh/id_rsa.pub
```
สามารถค้นหา google เรื่องการใช้ ssh config ด้วย keyword `ssh alias`

**แหล่งศึกษา:**

- [SSH Brute Force – The 10 Year Old Attack That Still Persists](https://blog.sucuri.net/2013/07/ssh-brute-force-the-10-year-old-attack-that-still-persists.html)
- [How to harden an SSH server?](http://askubuntu.com/a/2279)
- [Brute Force Key Attacks Are for Dummies](http://blog.codinghorror.com/brute-force-key-attacks-are-for-dummies/)