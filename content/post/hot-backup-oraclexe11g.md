+++
author = ""
comments = true
date = "2016-06-28T14:27:29+07:00"
draft = true
image = ""
menu = ""
share = true
slug = "hot-backup-oracle-xe_11g"
tags = ["development", "oracle"]
title = "แบ็คอัพ Oracle XE 11g แบบ developer"

+++

เนื่องจากลง Oracle XE เอาไว้พัฒนางานในเครื่องตัวเอง ตอนนี้อยากจะลอง เล่น software ตัวหนึ่งที่จะยุ่งเกี่ยวกับ database หากโชคร้ายงานของตัวเองหายไปก็จะแย่เอา เลยหาวิธี backup มันซะก่อนจะทดลองทำอะไรกับมัน 

การ backup เราสามารถจะทำเป็นแบบ text based หรือ binary based ได้ แบบ text based ก็คือ export มันออกมาเป็น script แต่วิธีออกมาเป็น script บางทีจะมีปัญหาเรื่อง encoding และจะ backup พวก field ที่เป็น BLOB, CLOB ไม่ได้ 

ในโพสนี้เราจะทำ backup แบบ binary กัน ศัพท์ในการ backup เป็น bจะมีคำว่า hot กับ cold

Hot backup คือการทำ backup ในขณะที่ database ยังเปิดใช้งานอยู่
Cold backup คือการทำ backup ตอนที่ database ปิดบริการ ไม่มีการเชื่อมต่อ

ภายใต้ folder ที่ติดตั้ง oracle-xe ไว้จะมีเครื่องมือการ backup มาให้อยู่แล้ว เราจะสนใจไฟล์นี้ 

`/u01/app/oracle/product/11.2.0/xe/config/scripts/backup.sh`

 เปลี่ยน user 
```
[root@centos-vm ~]# su - oracle
-bash-4.1$
```

```
-bash-4.1$ cd /u01/app/oracle/product/11.2.0/xe/
```

```
-bash-4.1$ . bin/oracle_env.sh
```

```
-bash-4.1$ ./sqlplus sys/manager as sysdba

SQL*Plus: Release 11.2.0.2.0 Production on Tue Jun 28 11:53:40 2016

Copyright (c) 1982, 2011, Oracle.  All rights reserved.


Connected to:
Oracle Database 11g Express Edition Release 11.2.0.2.0 - 64bit Production

SQL> shutdown immediate
Database closed.
Database dismounted.
ORACLE instance shut down.
SQL> startup mount
ORACLE instance started.

Total System Global Area  308981760 bytes
Fixed Size		    2226040 bytes
Variable Size		  264243336 bytes
Database Buffers	   37748736 bytes
Redo Buffers		    4763648 bytes
Database mounted.
SQL> alter database archivelog;

Database altered.

SQL> alter database open;

Database altered.

SQL> alter system set DB_RECOVERY_FILE_DEST_SIZE = 15G;

System altered.

SQL> exit
Disconnected from Oracle Database 11g Express Edition Release 11.2.0.2.0 - 64bit Production
-bash-4.1$ ./sqlplus / as sysdba

SQL*Plus: Release 11.2.0.2.0 Production on Tue Jun 28 11:56:57 2016

Copyright (c) 1982, 2011, Oracle.  All rights reserved.


Connected to:
Oracle Database 11g Express Edition Release 11.2.0.2.0 - 64bit Production

SQL> archive log list
Database log mode	       Archive Mode
Automatic archival	       Enabled
Archive destination	       USE_DB_RECOVERY_FILE_DEST
Oldest online log sequence     19
Next log sequence to archive   20
Current log sequence	       20
SQL> show parameter recover;

NAME				     TYPE	 VALUE
------------------------------------ ----------- ------------------------------
db_recovery_file_dest		     string	 /u01/app/oracle/fast_recovery_
						 area
db_recovery_file_dest_size	     big integer 15G
db_unrecoverable_scn_tracking	     boolean	 TRUE
recovery_parallelism		     integer	 0
SQL> exit
Disconnected from Oracle Database 11g Express Edition Release 11.2.0.2.0 - 64bit Production
-bash-4.1$ pwd
/u01/app/oracle/product/11.2.0/xe/bin
-bash-4.1$ cd ../config/scripts/
-bash-4.1$ ls -l
total 160
-rwxr-xr-x. 1 oracle dba   293 Aug 29  2011 backupdatabase.sh
-rwxr-xr-x. 1 oracle dba  6617 Aug 29  2011 backup.sh
-rwxr-xr-x. 1 oracle dba  1599 Aug 29  2011 cloneDBCreation.sql
-rwxr-xr-x. 1 oracle dba   283 Aug 29  2011 CloneRmanRestore.sql
-rwxr-xr-x. 1 oracle dba   605 Aug 29  2011 conmsg.sql
-rwxr-xr-x. 1 oracle dba   715 Nov 18  2015 gettingstarted.sh
-rwxr-xr-x. 1 oracle dba   214 Aug 29  2011 gotoonlineforum.sh
-rwxr-xr-x. 1 oracle dba  1722 Nov 17  2015 init.ora
-rwxr-xr-x. 1 oracle dba  1720 Nov 17  2015 initXETemp.ora
-rwxr-xr-x. 1 oracle dba 19592 Aug 29  2011 oracle-xe
-rwxr-xr-x. 1 oracle dba   913 Aug 29  2011 oraclexe.menu
-rwxr-xr-x. 1 oracle dba   148 Aug 29  2011 oraclexe-merge.menu
-rwxr-xr-x. 1 oracle dba 20237 Aug 29  2011 oracle-xe.sles
-rwxr-xr-x. 1 oracle dba   884 Nov 18  2015 postDBCreation.sql
-rwxr-xr-x. 1 oracle dba   535 Nov 17  2015 postScripts.sql
-rwxr-xr-x. 1 oracle dba   220 Aug 29  2011 readdocumentation.sh
-rwxr-xr-x. 1 oracle dba   259 Aug 29  2011 registerforonlineforum.sh
-rwxr-xr-x. 1 oracle dba   298 Aug 29  2011 restoredatabase.sh
-rwxr-xr-x. 1 oracle dba  5838 Aug 29  2011 restore.sh
-rwxr-xr-x. 1 oracle dba  1558 Aug 29  2011 rmanBackup.sql
-rwxr-xr-x. 1 oracle dba  1313 Aug 29  2011 rmanRestoreDatafiles.sql
-rwxr-xr-x. 1 oracle dba   432 Aug 29  2011 runsqlplus.sh
-rwxr-xr-x. 1 oracle dba   264 Aug 29  2011 sqlplus.sh
-rwxr-xr-x. 1 oracle dba  1211 Aug 29  2011 startdb.sh
-rwxr-xr-x. 1 oracle dba    33 Aug 29  2011 startdb.sql
-rwxr-xr-x. 1 oracle dba  1242 Aug 29  2011 stopall.sh
-rwxr-xr-x. 1 oracle dba  1027 Aug 29  2011 stopdb.sh
-rwxr-xr-x. 1 oracle dba    44 Aug 29  2011 stopdb.sql
-rwxr-xr-x. 1 oracle dba   543 Aug 29  2011 XE.sh
-rwxr-xr-x. 1 oracle dba   491 Aug 29  2011 XE.sql
-bash-4.1$ ./backup.sh
Doing online backup of the database.
Backup of the database succeeded.
Log file is at /u01/app/oracle/oxe_backup_current.log.
Press ENTER key to exit
-bash-4.1$ ls
backupdatabase.sh     gotoonlineforum.sh   oracle-xe.sles             restore.sh                startdb.sql
backup.sh             init.ora             postDBCreation.sql         rmanBackup.sql            stopall.sh
cloneDBCreation.sql   initXETemp.ora       postScripts.sql            rmanRestoreDatafiles.sql  stopdb.sh
CloneRmanRestore.sql  oracle-xe            readdocumentation.sh       runsqlplus.sh             stopdb.sql
conmsg.sql            oraclexe.menu        registerforonlineforum.sh  sqlplus.sh                XE.sh
gettingstarted.sh     oraclexe-merge.menu  restoredatabase.sh         startdb.sh                XE.sql
-bash-4.1$ pwd
/u01/app/oracle/product/11.2.0/xe/config/scripts
-bash-4.1$ ls -l
total 160
-rwxr-xr-x. 1 oracle dba   293 Aug 29  2011 backupdatabase.sh
-rwxr-xr-x. 1 oracle dba  6617 Aug 29  2011 backup.sh
-rwxr-xr-x. 1 oracle dba  1599 Aug 29  2011 cloneDBCreation.sql
-rwxr-xr-x. 1 oracle dba   283 Aug 29  2011 CloneRmanRestore.sql
-rwxr-xr-x. 1 oracle dba   605 Aug 29  2011 conmsg.sql
-rwxr-xr-x. 1 oracle dba   715 Nov 18  2015 gettingstarted.sh
-rwxr-xr-x. 1 oracle dba   214 Aug 29  2011 gotoonlineforum.sh
-rwxr-xr-x. 1 oracle dba  1722 Nov 17  2015 init.ora
-rwxr-xr-x. 1 oracle dba  1720 Nov 17  2015 initXETemp.ora
-rwxr-xr-x. 1 oracle dba 19592 Aug 29  2011 oracle-xe
-rwxr-xr-x. 1 oracle dba   913 Aug 29  2011 oraclexe.menu
-rwxr-xr-x. 1 oracle dba   148 Aug 29  2011 oraclexe-merge.menu
-rwxr-xr-x. 1 oracle dba 20237 Aug 29  2011 oracle-xe.sles
-rwxr-xr-x. 1 oracle dba   884 Nov 18  2015 postDBCreation.sql
-rwxr-xr-x. 1 oracle dba   535 Nov 17  2015 postScripts.sql
-rwxr-xr-x. 1 oracle dba   220 Aug 29  2011 readdocumentation.sh
-rwxr-xr-x. 1 oracle dba   259 Aug 29  2011 registerforonlineforum.sh
-rwxr-xr-x. 1 oracle dba   298 Aug 29  2011 restoredatabase.sh
-rwxr-xr-x. 1 oracle dba  5838 Aug 29  2011 restore.sh
-rwxr-xr-x. 1 oracle dba  1558 Aug 29  2011 rmanBackup.sql
-rwxr-xr-x. 1 oracle dba  1313 Aug 29  2011 rmanRestoreDatafiles.sql
-rwxr-xr-x. 1 oracle dba   432 Aug 29  2011 runsqlplus.sh
-rwxr-xr-x. 1 oracle dba   264 Aug 29  2011 sqlplus.sh
-rwxr-xr-x. 1 oracle dba  1211 Aug 29  2011 startdb.sh
-rwxr-xr-x. 1 oracle dba    33 Aug 29  2011 startdb.sql
-rwxr-xr-x. 1 oracle dba  1242 Aug 29  2011 stopall.sh
-rwxr-xr-x. 1 oracle dba  1027 Aug 29  2011 stopdb.sh
-rwxr-xr-x. 1 oracle dba    44 Aug 29  2011 stopdb.sql
-rwxr-xr-x. 1 oracle dba   543 Aug 29  2011 XE.sh
-rwxr-xr-x. 1 oracle dba   491 Aug 29  2011 XE.sql
-bash-4.1$ pwd
/u01/app/oracle/product/11.2.0/xe/config/scripts
-bash-4.1$ cd ../..
-bash-4.1$ ls -l
total 96
drwxr-xr-x.  2 oracle dba 4096 Nov 17  2015 apex
drwxr-xr-x.  2 oracle dba 4096 Nov 17  2015 bin
drwxr-xr-x.  4 oracle dba 4096 Nov 18  2015 config
drwxr-xr-x.  9 oracle dba 4096 Nov 17  2015 ctx
drwxr-xr-x.  2 oracle dba 4096 Jun 28 11:58 dbs
drwxr-xr-x.  3 oracle dba 4096 Nov 17  2015 demo
drwxr-xr-x.  4 oracle dba 4096 Nov 17  2015 hs
drwxr-xr-x.  3 oracle dba 4096 Nov 17  2015 jdbc
drwxr-xr-x.  2 oracle dba 4096 Nov 17  2015 jlib
drwxr-xr-x.  3 oracle dba 4096 Nov 17  2015 ldap
drwxr-xr-x.  2 oracle dba 4096 Nov 17  2015 lib
drwxr-xr-t.  4 oracle dba 4096 Nov 18  2015 log
drwxr-xr-x.  3 oracle dba 4096 Nov 17  2015 md
drwxr-xr-x.  8 oracle dba 4096 Nov 17  2015 network
drwxr-xr-x.  5 oracle dba 4096 Nov 17  2015 nls
drwxr-xr-x.  7 oracle dba 4096 Nov 17  2015 odbc
drwxr-xr-x.  4 oracle dba 4096 Nov 17  2015 opmn
drwxr-xr-x.  4 oracle dba 4096 Nov 17  2015 oracore
drwxr-xr-x.  4 oracle dba 4096 Nov 17  2015 plsql
drwxr-xr-x.  4 oracle dba 4096 Nov 17  2015 precomp
drwxr-xr-x. 12 oracle dba 4096 Nov 17  2015 rdbms
drwxr-xr-x.  3 oracle dba 4096 Nov 17  2015 slax
drwxr-xr-x.  4 oracle dba 4096 Nov 17  2015 sqlplus
drwxr-xr-x.  6 oracle dba 4096 Nov 17  2015 xdk
-bash-4.1$ cd bin
-bash-4.1$ ls -l
total 191312
-rwxr-xr-x. 1 oracle dba     13091 Aug 29  2011 adapters
-rwxr-xr-x. 1 oracle dba      8614 Aug 29  2011 createdb.sh
-rwxr-xr-x. 1 oracle dba    523832 Aug 29  2011 ctxkbtc
-rwxr-xr-x. 1 oracle dba    453648 Aug 29  2011 ctxlc
-rwxr-xr-x. 1 oracle dba    193080 Aug 29  2011 ctxload
-rwxr-xr-x. 1 oracle dba      6840 Aug 29  2011 cursize
-rwxr-xr-x. 1 oracle dba    558768 Aug 29  2011 dbfs_client
-rwxr-xr-x. 1 oracle dba      6856 Aug 29  2011 dbfsize
-rwxr-xr-x. 1 oracle dba      2415 Aug 29  2011 dbhome
-rwxr-xr-x. 1 oracle dba    321904 Aug 29  2011 dbv
-rwxr-xr-x. 1 oracle dba    340072 Aug 29  2011 dg4odbc
-rwxr-xr-x. 1 oracle dba    349320 Aug 29  2011 dgmgrl
-rwxr-xr-x. 1 oracle dba      8296 Aug 29  2011 dumpsga
-rwxr-xr-x. 1 oracle dba    614568 Aug 29  2011 exp
-rwxr-xr-x. 1 oracle dba    126440 Aug 29  2011 expdp
-rwxr-xr-x. 1 oracle dba    608048 Aug 29  2011 extjob
-rwxr-xr-x. 1 oracle dba    608048 Aug 29  2011 extjobo
-rwxr-xr-x. 1 oracle dba      5944 Aug 29  2011 extproc
-rwxr-xr-x. 1 oracle dba    300968 Aug 29  2011 imp
-rwxr-xr-x. 1 oracle dba    136904 Aug 29  2011 impdp
-rwxr-xr-x. 1 oracle dba    368456 Aug 29  2011 kgmgr
-rwxr-xr-x. 1 oracle dba     12536 Aug 29  2011 loadpsp
-rwxr-xr-x. 1 oracle dba     91016 Aug 29  2011 lsnrctl
-rwxr-xr-x. 1 oracle dba    511824 Aug 29  2011 lxchknlb
-rwxr-xr-x. 1 oracle dba      7960 Aug 29  2011 mapsga
-rwxr-xr-x. 1 oracle dba      6872 Aug 29  2011 maxmem
-rwxr-xr-x. 1 oracle dba     58968 Aug 29  2011 nid
-rwxr-xr-x. 1 oracle dba     16759 Aug 29  2011 nls_lang.sh
-rwsr-s--x. 1 oracle dba 165700472 Aug 29  2011 oracle
-rwxr-xr-x. 1 oracle dba       156 Aug 29  2011 oracle_env.csh
-rwxr-xr-x. 1 oracle dba       156 Aug 29  2011 oracle_env.sh
-rwxr-xr-x. 1 oracle dba     52800 Aug 29  2011 oradism
-rwxr-xr-x. 1 oracle dba      6183 Aug 29  2011 oraenv
-rwxr-xr-x. 1 oracle dba     15232 Aug 29  2011 orapwd
-rwxr-xr-x. 1 oracle dba     19936 Aug 29  2011 osdbagrp
-rwxr-xr-x. 1 oracle dba     20040 Aug 29  2011 osh
-rwxr-xr-x. 1 oracle dba       946 Aug 29  2011 ott
-rwxr-xr-x. 1 oracle dba  14087984 Aug 29  2011 rman
-rwxr-xr-x. 1 oracle dba   1313712 Aug 29  2011 sqlldr
-rwxr-x--x. 1 oracle dba      4968 Aug 29  2011 sqlplus
-rwxr-xr-x. 1 oracle dba     13352 Aug 29  2011 sysresv
-rwxr-xr-x. 1 oracle dba     85096 Aug 29  2011 tkprof
-rwxr-xr-x. 1 oracle dba    738440 Aug 29  2011 tnslsnr
-rwxr-xr-x. 1 oracle dba     11040 Aug 29  2011 tnsping
-rwxr-xr-x. 1 oracle dba     16496 Aug 29  2011 trcroute
-rwxr-xr-x. 1 oracle dba    144784 Aug 29  2011 unzip
-rwxr-xr-x. 1 oracle dba   7097808 Aug 29  2011 wrap
-rwxr-xr-x. 1 oracle dba    208384 Aug 29  2011 zip
-bash-4.1$
```



**แหล่งศึกษา:**

* [How to backup an oracle xe database](http://www.thefreyers.net/doku.php?id=technology:databases:how_to_backup_an_oracle_xe_database)
* [Oracle® Database Express Edition 2 Day DBA](https://docs.oracle.com/cd/B25329_01/doc/admin.102/b25107/backrest.htm)
* [Oracle XE backup scripts](https://github.com/schmooser/oracle-xe-backup)
