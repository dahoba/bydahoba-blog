+++
date = "2016-01-26T16:55:36+07:00"
draft = false
title = "แบ็คอัพงานแบบมีหลักการ ด้วย 3-2-1 Backup plan"
slug = "backup-your-work-with-321-plan"
image = ""
menu = ""
share = true
tags = ["productivity"]
+++
![backup-your-work poster](https://i.imgur.com/xYEyi0cl.jpg)

คุณทำงานกับคอมพิวเตอร์แล้วสำรองข้อมูลกันบ้างหรือเปล่า? 

ถ้าไม่ backup เลย เหมือนกับเราเล่นกระโดดร่มแล้วไม่มีร่มชูชีพ แต่หากทำ backup แต่ทำเพียงชุดเดียวมันก็อันตรายและเป็นการทำ backup ที่ไม่มีประสิทธิภาพอีกด้วย หาก hard disk ลูกนั้นมันเกิดเสียขึ้นมา โดนขโมย หรือทำหาย ทำหล่นจากที่สูง ฯลฯ สิ่งที่คุณ backup ไว้ก็จะสูญ! ตลอดกาล การสร้าง backup เอาไว้มากๆ ก็เปลืองเวลาเปลืองค่าใช้จ่าย เราเดินทางสายกลางทำแต่พอดีและมีประสิทธิภาพดีกว่า

<!--more-->

เค้ามีสูตรที่ทำตามๆ กันมา(ไม่รู้ว่าต้นฉบับมาจากใคร อย่างไร) เรียกว่า "แผน 3-2-1" เป็นแผนที่เข้าท่าและมีหลักการดีครับ

**เลข 3** มาจาก เวลาทำ backup ให้มี 3 copies หรือ 3 ชุด หากเกิดเหตุไม่คาดฝัน backup ตัวที่ 2 เกิดพังใช้ไม่ได้ เราก็ยังมีตัวที่ 3 ให้อุ่นใจอยู่ (หากมันจะซวยกว่านี้ ให้มันรู้กันไปสิ่ ถถ..ถ)

Primary copy หรือจะเรียกว่า copy ที่ 1 เราจะใช้ hard disk เพราะราคาไม่แพง หาซื้อได้ง่าย อ่านเขียนก็ไว้ ตัวนี้เราใช้ backup ได้ทุกวันทั้งวันเลย

**เลข 2** มาจาก ให้ backup ใส่ media ที่แตกต่างกัน 2 ชนิด media ที่ว่านี้เช่น hard disk, flash drive, DVD/Blue-ray disk, tape backup การ backup ใส่ media 2 ชนิด เช่น copy แรก hard disk copy ที่สอง DVD หรือ hard disk กับ tape backup 

สังเกตว่า flashdrive, dvd, tape สื่อพวกนี้ไม่ต้องใช้ไฟเลี้ยง ข้อมูลก็อยู่ได้นาน  และพวกนี้เป็น removable media คือ สื่อที่ถอดเข้าออกได้ครับ ซึ่งจะนำไปสู่ เลขสุดท้าย 

**เลข 1** คือเมื่อเราทำ copy 3 ชุดแล้ว ถ้าจะให้ปลอดภัยสุดๆตามแผนนี้ ให้เอา copy 1 ชุดไปไว้ที่อื่น (off-site) ที่ที่ไม่ใช่ที่ทำงานของเรา (ก็คือ ถ้าทำงานที่บ้านก็ไม่เก็บไว้ที่บ้านน่ะ) เมื่อเกิดเหตุขโมย,ไฟไหม้,น้ำท่วม เราก็ยังจะมี backup ตัวสุดท้ายนี้อยู่ 

เขาบอกว่าเมื่อเอา backup copy หนึ่งอยู่ใน removable media แล้วทำให้เราส่งมันไปไว้ที่อื่นได้ง่าย ข้อนี้ คิดว่าสมัยที่คิดแผนนี้ออกมา cloud storage ยังมีราคาแพง และเข้าถึงกันได้ยาก ถ้าเป็นสมัยนี้ ตัว off-site นี้ อาจจะเป็น cloud drive ก็ได้ 

อ่านหลักการกันไปแล้ว มาถึงภาคปฎิบัติ นำเสนอแบบนี้ครับ 

**Macbook Backup Plan**

![suggested plan image](https://i.imgur.com/VkDM7E1m.jpg)

**Primary copy**  
ใช้ External hard disk WD Passport 1 TB ทำ Time Machine 

**Secondary copy**  
ทุกสัปดาห์ backup Time machine ใน primary ไปที่ NAS หรือ hard disk ตัวที่ 2 

**Third copy**  
off-site backup ใช้บริการ cloud storage ที่ดูไว้ [CrashPlan](https://store.code42.com/store/) ดูเข้าท่ามาก

![crashplan img](https://i.imgur.com/56n59Ggl.png)

* มี software ให้ใช้ทำ local backup FREE 
* รองรับทั้ง 3 platform OSX, Linux, Window
* unlimit backup space
* ปีละ 2,152 บาท ($59.99)

![crashplan free](https://i.imgur.com/ZhX34wMl.png) ![crashplan cloud pricing](https://i.imgur.com/eXnahifl.png)

ส่วน Cloud รายอื่นๆ ที่น่าสนใจ (ไม่นับ Dropbox, Google Drive, OneDrive แล้ว) ลองกดไปศึกษารายละเอียดกันได้ 

* [Carbonite](http://www.carbonite.com/en/cloud-backup/personal-solutions/personal-plans/buy/) ปีละ 2,152 บาท ($59.99) ไม่รองรับ linux
* [Acronis Home+cloud](http://www.acronis.com/en-us/personal/true-image-comparison/) ตัวนี้แพงหน่อย 3,588 บาท/ปี ($99.99) แต่รวม mobile backup (3 เครื่อง) อยู่ด้วย 
* [iDrive](https://www.idrive.com/pricing) ตัวนี้จะคิดค่าบริการตามพื้นที่เริ่มต้นที่ 1 TB หากเรารู้ว่าข้อมูล backup เราจะไม่เกิน 1 TB ก็จะได้ค่าบริการที่ถูกหน่อย เริ่มต้นที่ 1,615บ./ปี ($45)

ท่านผู้อ่านวางแผน backup ข้อมูล/งานของท่านอย่างไรกันบ้าง มา share กันครับ 


แหล่งศึกษาเพิ่มเติม:

* [ultimate-backup-guide](http://www.bestbackups.com/ultimate-backup-guide/)  
* [@FordAntiTrust's แนวทางการ Backup ข้อมูล](https://www.thaicyberpoint.com/ford/blog/id/1141/)  
* [tutsplus's Data Backups and Storage](http://computers.tutsplus.com/series/data-backups-and-storage--mac-52613)
 
poster's background image from [pixabay](https://pixabay.com/en/usb-memory-card-cd-data-flash-932180/) 