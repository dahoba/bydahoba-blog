+++
author = "Siritas S."
comments = true
date = "2016-03-09T09:47:24+07:00"
draft = false
image = ""
menu = ""
share = true
slug = "guide-android-app-connect-google-drive-api"
tags = ["development", "android"]
title = "Android: ขั้นตอนการใช้ Google Drive API กับ Android app"

+++

<img src="https://i.imgur.com/zJOdB6V.jpg" width="640" />

มีเพื่อนมาขอคำแนะนำ เค้าเขียนแอพขึ้นมา มี feature หนึ่งจะเอาไฟล์ไปใส่ใน Drive
ติดที่ว่าพอขึ้นหน้าจอเลือก account แล้วมันก็หมุนวนไปวนมาอยู่อย่างนั้น หลังจากทดลองทำสำเร็จแล้ว เลยมาบันทึกไว้หน่อย
<!--more-->

**ภาพรวมของขั้นตอนที่เราจะต้องทำ**

1. ติดตั้ง Android SDK ซะก่อน
2. ดาวโหลดและติดตั้ง Google Play services SDK ตัวนี้แหละจะมี Drive Android API มาให้ด้วย
3. จะต้องไปบอก Drive API ว่าเราจะเชื่อมต่อจากแอพไหนของเรา ที่ Google Developers Console
4. จากขั้นตอนที่ 3 เราจะได้ APP_ID (หรือชื่อเป็นทางการว่า signing certificate) จะต้องเอามาใส่ใน AndroidManifest.xml ในแอพเรา
5. เขียนโค้ดเพื่อทดสอบ ง่ายที่สุดคือการสร้างไฟล์ใน Drive (หรือก็คือการ upload file ขึ้นไปนั่นเอง)

อ้างอิงตาม [เอกสาร ของ Google Developer](https://developers.google.com/drive/android/get-started#overview)

**ข้อ 1**

จะข้ามไปนะครับ ถ้าเขียนแอพ Android แล้วถือว่าต้องทำไปแล้ว, ถ้ายัง ตามไปทำได้จากที่นี่ [Get the Android SDK](https://developer.android.com/sdk/index.html) ขั้นตอนง่ายๆไม่ซับซ้อน

**ข้อ 2**

ติดตั้ง Google Play services SDK [ขั้นตอนนี้](https://developers.google.com/android/guides/setup#add_google_play_services_to_your_project)

ก็มีข้อสังเกตว่า `build.gradle` เราจะเอาที่อยู่ใน Module ไม่ใช่ตัวที่เป็นของ Project

![build.gradle sample](https://i.imgur.com/AwV72WB.png)

2.2 ตัวอย่างเป็นแบบนี้

```json
apply plugin: 'com.android.application'
    ...
    dependencies {
        compile 'com.google.android.gms:play-services:8.4.0'
    }
```

แต่เราจะอยากได้เฉพาะ Drive API ใช้แบบนี้แทนได้

```json
apply plugin: 'com.android.application'
    ...

    dependencies {
        compile 'com.google.android.gms:play-services-drive:8.4.0'
    }
```

แก้ไขแล้วกด Save มันจะขึ้นมาเตือนให้เรา อัพเดต project เนื่องจากไฟล์ gradle มีการแก้ไข กด Sync ไปครับ

![gradle sync alert](https://i.imgur.com/dhtixP4.png)

**ข้อ 3**

เราจะไป register แอพที่เราจะใช้งานกับ Google Drive API กดไปที่ [Developer console](https://console.developers.google.com/apis/credentials)

ถ้าเรายังไม่มี Project ในนี้มันจะให้เราสร้าง project คิดชื่อมาซักชื่อนึง
แน่นอนครับคำถามสุดท้าย ต้องตอบ Yes แล้วเราจะ Create project ได้

กด Create

![create project](https://i.imgur.com/7Cd31xe.png)

เลือก Use Google APIs / Enable and manage APIs

![google apis](https://i.imgur.com/DtriYPw.png)

เลือก Enable Drive API จะพิมพ์ในช่องค้นหา หรือไล่ๆหา link แล้วกดมา จะได้หน้าจอแบบนี้ กด Enable

![drive apis](https://i.imgur.com/mkfEe0z.png)

พอกดเสร็จจะเห็นว่ามันจะบอกว่ายังใช้ไม่ได้นะต้องไปสร้าง credentials ก่อน กด Go to Credentials

![warning create credentials](https://i.imgur.com/SnohXYa.png)

3.1 หน้าจอนี้เพื่อสร้าง credentials เราจะต้องเลือกว่าแอพเราจะทำอย่างไรกับ Drive API

เลือกได้สองแบบ

* **User data** : อ่านเขียนไฟล์ที่เป็นเจ้าของ Drive
* **Application data** : อ่านเขียนไฟล์ของตัวแอพเอง เช่นการเก็บข้อมูลของแอพใส่ Drive ของผู้ใช้ ไปอ่านไฟล์อื่นๆไม่ได้ <u style="color:#4CAF50">แต่เลือกอันนี้ไม่ได้</u> Google ไม่ยอม บอกว่าตอนนี้ยังไม่ปลอดภัย

![credential question](https://i.imgur.com/EIhuOc3.png)


3.2 หน้าจอนี้ให้เราเอา SHA-1 ของ keystore ที่ต้องการมากรอก และใส่ app id ของเราด้วย

![Imgur](https://i.imgur.com/6bQGr8W.png?1)

**วิธีเอา SHA-1** จาก keystore

พิมพ์คำสั่งแบบนี้ใน terminal หรือ cmd หากใช้ Windows

```sh
$ keytool -exportcert -keystore debug.keystore -list -v
```

`debug.keystore` เป็นของที่ได้มาจากการติดตั้ง Android SDK keystore password ถูกกำหนดมาเหมือนกันหมดคือ `android`

<i style="color:#00796B">สำหรับแอพที่จะเอาลง Play store เราจะต้องทำ keystore ใหม่อีกตัวนะครับ เวลาพัฒนาใช้ตัวนึง เวลาจะปล่อยของก็ใช้อีกตัวนึง ต้องสร้าง credentials สองชุด</i>

เมื่อใส่และกด Enter แล้วจะได้ผลลัพธ์ออกมาประมาณนี้

```
Enter keystore password:

Keystore type: JKS
Keystore provider: SUN

Your keystore contains 1 entry

Alias name: androiddebugkey
Creation date: Dec 23, 2015
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: CN=Android Debug, O=Android, C=US
Issuer: CN=Android Debug, O=Android, C=US
Serial number: 32dd2296
Valid from: Wed Dec 23 11:54:49 ICT 2015 until: Fri Dec 15 11:54:49 ICT 2045
Certificate fingerprints:
	 MD5:  6A:9D:E3:47:83:12:F7:E2:BD:21:06:34:78:2D:58:1B
	 SHA1: F9:46:02:13:33:6F:27:53:7B:9D:95:8B:2C:DE:9D:19:53:0B:01:E8
	 SHA256: CA:5A:9A:D8:91:E7:A6:1E:C9:E2:19:F6:23:DF:BF:25:68:BF:3D:AC:CA:D5:33:44:26:DD:EF:4F:1E:1F:AA:97
	 Signature algorithm name: SHA256withRSA
	 Version: 3

Extensions:

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 87 F5 F1 D1 8E 70 F8 D1   9F C8 10 CB BA 85 40 C0  .....p........@.
0010: DA 01 CA CA                                        ....
]
]



*******************************************
*******************************************
```

ให้มองหา SHA1 จากตัวอย่างข้างบนจะได้ เลข fingerprint id นี้ `F9:46:02:13:33:6F:27:53:7B:9D:95:8B:2C:DE:9D:19:53:0B:01:E8`

**Package name** หาได้จาก AndroidManifest.xml ดู `package=`

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="xyz.dahoba.drivesample" >
```

กรอกครบแล้ว กด Create

3.3 หลังจากนั้นจะมาถึงขั้นตอนที่ 3

![consent screen](https://i.imgur.com/EVswqDV.png)

Email ก็จะเป็นของเราเอง ส่วน Product name ก็ใส่ชื่อแอพเรา เช่น `Mars Sample`

กด Continue

ครบขั้นตอนจะได้ OAuth client id มาแล้ว ถ้ามัน pop-up ให้ดาวโหลดก็เซฟเก็บไว้ครับ

![oauth](https://i.imgur.com/qe5wqkz.png?1)

**ข้อ4**

เราจะได้ Client ID มาแล้ว ต่อไปจะต้องเพิ่มโค้ดใน AndroidManifest.xml ในโปรเจคเรา

**โค้ดตัวอย่างด้านล่าง เอามาจาก github [Google Drive Android Quickstart](https://github.com/googledrive/android-quickstart)**

ไฟล์ก่อนแก้ไข

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.google.android.gms.drive.sample.quickstart"
    android:versionCode="1"
    android:versionName="1.0" >

    <uses-sdk
        android:minSdkVersion="8"
        android:targetSdkVersion="18" />

    <application
        android:allowBackup="true"
        android:icon="@drawable/ic_launcher"
        android:label="@string/app_name"
        android:theme="@style/AppTheme" >
        <activity
            android:name="com.google.android.gms.drive.sample.quickstart.MainActivity"
            android:label="@string/app_name" >
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
        <meta-data
            android:name="com.google.android.gms.version"
            android:value="@integer/google_play_services_version" />
    </application>

</manifest>
```

เราจะต้องเพิ่ม tag เหล่านี้เข้าไป

**uses-permission** ใส่ข้างนอก `<application>`

```xml
<uses-permission android:name="android.permission.GET_ACCOUNTS" />
```

**meta-data** ใส่ใน `<activity>` ที่จะเป็นตัวเรียก Drive API

สังเกต `id=1234567890` นะครับเราจะเอา Client ID มาใส่ที่นี่

```xml
<meta-data android:name="com.google.android.apps.drive.APP_ID" android:value="id=1234567890" />
```

**action** DRIVE_OPEN กับ **data** mimeType ใส่ใน `<intent-filer>`

```xml
<action android:name="com.google.android.apps.drive.DRIVE_OPEN" />
<data android:mimeType="image/png" />
<data android:mimeType="image/jpeg" />
<data android:mimeType="image/jpg" />
```

ไฟล์ที่แก้ไขแล้ว หากจะเอาไปใส่ใน project ของผู้อ่านเองก็สังเกต tag ทั้งหมด ที่ว่ามานะครับ

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.google.android.gms.drive.sample.quickstart"
    android:versionCode="1"
    android:versionName="1.0" >

    <uses-sdk
        android:minSdkVersion="8"
        android:targetSdkVersion="18" />

    <uses-permission android:name="android.permission.GET_ACCOUNTS" />

    <application
        android:allowBackup="true"
        android:icon="@drawable/ic_launcher"
        android:label="@string/app_name"
        android:theme="@style/AppTheme" >
        <activity
            android:name="com.google.android.gms.drive.sample.quickstart.MainActivity"
            android:label="@string/app_name" >
            <meta-data android:name="com.google.android.apps.drive.APP_ID" android:value="122478124018-viukforjq2fj06kqpqsgq38rjnxxxxxx.apps.googleusercontent.com" />
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />
                <action android:name="com.google.android.apps.drive.DRIVE_OPEN" />
                <data android:mimeType="image/png" />
                <data android:mimeType="image/jpeg" />
                <data android:mimeType="image/jpg" />
                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
        <meta-data
            android:name="com.google.android.gms.version"
            android:value="@integer/google_play_services_version" />
    </application>

</manifest>
```

Compile project แล้วลองทดสอบดู _จะต้องเป็น device หรือ emulator ที่มี Play service อยู่นะครับถึงจะใช้งานได้ครับ_
