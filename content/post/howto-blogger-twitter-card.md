+++
author = "siritas"
comments = true
date = "2016-02-12T10:43:23+07:00"
draft = false
image = ""
menu = ""
share = true
slug = "howto-blogger-opengraph-and-twitter-card"
tags = ["tips", "blogger"]
title = "แชร์เนื้อหา blog ให้สวยงามใน timeline ด้วย Twitter card และ Facebook Open Graph"

+++
![poster](https://lh3.googleusercontent.com/-yWdsEQrdDng/Vr14Eb_yTJI/AAAAAAACMYo/9tXY-dpYltg/s640-Ic42/blogger_twitter-card_faecbook-og_poster.jpg)

ปีนี้อยากจะเขียน blog ให้เยอะๆ พอเริ่มเขียนเยอะเข้าก็เจอเรื่องขัดใจที่ว่า เวลา share เนื้อหาใน twitter หรือ facebook แล้ว ทำไม URL เรามันไม่แสดงสวยๆเหมือนที่คนอื่นเค้าทำกัน ค้นไปค้นมา อ๋อ มันมี keyword เราต้องใช้ Twitter Card และ Facebook Open Graph

ใน Blogger มันมี widget ให้ใช้แต่มันก็เหมือนว่าจะไม่สมบูรณ์ ไม่เหมือน plugin ที่ Wordpress เค้ามีฝังมาให้ใน theme จริงๆแล้วเราต้องเพิ่ม meta information พวกนี้เอง เรามาดูกันว่าทำยังไง ใน blogger platform 
<!--more-->
ให้เข้าไปที่ Blogger dashboard > Template > (Live on Blog) Edit HTML

![edit HTML](https://lh3.googleusercontent.com/-prG9K5sxg6c/Vr1eSjmj3sI/AAAAAAACMYI/oTc-YlVyajk/s400-Ic42/blogger_edit_HTML.png)

ที่ editor ตรงกลาง

![editor image](https://lh3.googleusercontent.com/-bvWTFE9KsDA/Vr1e81wF1EI/AAAAAAACMYU/EoZHrO--BXE/s640-Ic42/blogger_edit_template.png) 

ให้ค้นหา tag `</head>`

```html
...
 </b:template-skin>
    <b:include data='blog' name='google-analytics'/>
  </head>
...
```

สำหรับ Facebook open graph ให้ใช้ code ด้านล่างนี้ 

```html
<!-- Open Graph Meta Tags BEGIN -->
<meta expr:content='data:blog.pageName' property='og:title'/>
  <b:if cond='data:blog.postImageThumbnailUrl'>
     <meta expr:content='data:blog.postImageThumbnailUrl' property='og:image'/>
  </b:if>
<meta expr:content='data:blog.canonicalUrl' property='og:url'/>
<b:if cond='data:blog.metaDescription'>
     <meta expr:content='data:blog.metaDescription' property='og:description'/>
</b:if>
<meta content='{ต้องไปสร้าง app ใน facebook developer เพื่อเอาเลข admins มา}' property='fb:admins'/>
<meta content='{ต้องไปสร้าง app ใน facebook developer เพื่อเอา app_id มา} ' property='fb:app_id'/>
<!-- Open Graph Meta Tags END -->
```

ตัวเลขใน content ของ `fb:admins` `fb:app_id` เป็นของใครของมันนะครับ 

สำหรับ Twitter card ใช้ code นี้ 

```html
<!-- twitter card meta tags BEGIN -->
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@{ใส่ twitter id}" />
<meta name="twitter:title" expr:content='data:blog.pageName' />
<meta name='twitter:description' expr:content='data:blog.metaDescription' />
<meta name="twitter:image" expr:content='data:blog.postImageThumbnailUrl' />
<!-- twitter card END-->
```

เมื่อใส่แล้วจะได้ประมาณนี้

```html
 </b:template-skin>
    <b:include data='blog' name='google-analytics'/>
<!-- Open Graph Meta Tags BEGIN -->
    <meta expr:content='data:blog.pageName' property='og:title'/>
    <b:if cond='data:blog.postImageThumbnailUrl'>
      <meta expr:content='data:blog.postImageThumbnailUrl' property='og:image'/>
    </b:if>
    <meta expr:content='data:blog.canonicalUrl' property='og:url'/>
    <b:if cond='data:blog.metaDescription'>
      <meta expr:content='data:blog.metaDescription' property='og:description'/>
    </b:if>
    <meta content='16XXXX5854' property='fb:admins'/>
    <meta content='172XXXX787484648 ' property='fb:app_id'/>
<!-- Open Graph Meta Tags END -->
<!-- twitter card meta tags BEGIN -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@twitterId" />
    <meta name="twitter:title" expr:content='data:blog.pageName' />
    <meta name='twitter:description' expr:content='data:blog.metaDescription' />
    <meta name="twitter:image" expr:content='data:blog.postImageThumbnailUrl' />
<!-- twitter card END-->
  </head>
```

ตัว `twitter:card` เป็นประเภทของการนำ url ของเราแสดงใน twitter timeline ดูแบบอื่นๆ ได้จากเอกสาร [Twitter's Card Types](https://dev.twitter.com/cards/types/summary)

เมื่อแก้ไขเสร็จแล้วกด `Save template` ลอง copy URL ของ post นึงของเราเพื่อทดสอบว่าใช้ได้หรือยัง 

สำหรับ twitter card ทดสอบที่ URL นี้ 
[https://cards-dev.twitter.com/validator](https://cards-dev.twitter.com/validator)

ตัวอย่างผลการทดสอบ twitter's card

![twitter's card](https://lh3.googleusercontent.com/-bTf3y0XsQqA/Vr1be5XJ_VI/AAAAAAACMXw/J8G47Lf-dnU/s640-Ic42/twitter_validate_sample.png)

สำหรับ Facebook opengraph ทดสอบที่ URL นี้ 
[https://developers.facebook.com/tools/debug/og/object/](https://developers.facebook.com/tools/debug/og/object/)

ตัวอย่างผลการทดสอบ  facebook's og

![facebook's og](https://lh3.googleusercontent.com/-LdDTgEHwSUo/Vr1bjltIRCI/AAAAAAACMXs/V-LTAgKVTHA/s400-Ic42/facebook-og_validate_sample.png)

**สุดท้ายฝากข้อสังเกต** 

`data:blog.postImageThumbnailUrl` ตัวแปรนี้เป็น URL ของภาพ thumbnail ของ post หนึ่งๆของเรา แสดงภาพในตัว share card ของเรา แต่ถ้าภาพที่เราใส่เข้าไปใน post ไม่ได้ upload ไปที่ picasa หรือ upload ผ่าน blogger มันจะกลายเป็นค่าว่าง ซะหยั่งงั้น 

`data:blog.metaDescription` ตัวแปรนี้เป็นคำบรรยายของ post หนึ่งๆของเรา ใส่ตรง `Search description` เวลาสร้าง post ใน blogger ถ้าไม่ใส่ ใน twitter หรือ facebook จะไปดึงเอาเนื้อหาของเรามาแสดง ซึ่งมันอาจจะดูไม่สวยเวลาอยู่ใน timeline นะครับ 
