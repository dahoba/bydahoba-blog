+++
author = "+SiritasS"
comments = true
date = "2016-09-07T16:25:35+07:00"
draft = false
image = ""
menu = ""
share = true
slug = "collected-css-styleguide"
tags = ["development"]
title = "รวบรวม CSS style guide ของทีมพัฒนา product/service ดังๆ"

+++
จะพัฒนาแอพให้ดูแล source code กันง่ายๆ ก็ไม่ใช่เรื่องง่าย เราควรจะเรียนรู้จากเหล่ามือโปรซึ่งแสดงผลลัพธ์อยู่ใน สินค้าหรือบริการที่พวกเค้าพัฒนากันออกมา โพสต์นี้เน้นเรื่อง CSS โดยเฉพาะ 

<!--more-->

# CSS Style Guide
### รวบรวม CSS style guide ของทีมพัฒนา product/service ดังๆ


[Primer](http://primercss.io/) -- The CSS toolkit and guidelines that power GitHub. ตัวนี้กดลิ้งมาจากของ Github นั่นแหละ [Styleguide · GitHub](https://github.com/styleguide) ใครสนใจ style guide ของ Ruby ตามไปต่อได้

[Airbnb](https://github.com/airbnb/css) -- A mostly reasonable approach to CSS and Sass.

[css{guide:lines;}](http://cssguidelin.es/) -- High-level advice and guidelines for writing sane, manageable, scalable CSS

[mdo’s](http://codeguide.co/) -- แนวทางการเขียน HTML CSS จากผู้สร้าง Twitter bootstrap 

[Google HTML/CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.xml) -- จาก Google

[Dropbox’s](https://github.com/dropbox/css-style-guide) -- จากทีมพัฒนา Dropbox

[Mailchimp’s](http://ux.mailchimp.com/patterns)  -- จาก Mailchimp ผู้ให้บริการ email subscribing รายใหญ่ ของเค้าน่าสนใจ น่าศึกษาไม่น้อย

[styleguides.io](http://styleguides.io/examples.html) -- อื่นๆอีกมากมาย เวบนี้รวบรวมไอเดียไว้เป็นแรงบันดาลใจเอาไว้เยอะมากๆ 