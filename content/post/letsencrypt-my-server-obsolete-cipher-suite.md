+++
author = "SiritasS"
comments = true
date = "2016-01-12T09:47:24+07:00"
draft = false
image = ""
menu = ""
share = true
slug = "letsencrypt-my-server-obsolete-cipher-suite"
tags = ["development"]
title = "Let's Encrypt: แก้ obsolete cipher suite ให้เป็น modern cipher suite"

+++
<img style="margin:0; margin-left:10%" src="https://lh3.googleusercontent.com/-6Q8-Go-qb7g/Vo83v6Hq0-I/AAAAAAACKwE/uVdUV3qb1X0/s400-Ic42/LE_fix_obsolete_cipher.jpg" />


ผมลองกดกุญแจเขียว แล้วเห็นคำว่า **obsolete cipher suite**  มันทำให้ข้องใจ ทำ SSL website ของเราได้แล้วแต่เหมือนทำไม่เสร็จ
มันเป็นเพราะ configuration ของ http server ของเรา
<!--more-->

![Imgur](https://i.imgur.com/KpTmGzF.png)

![Imgur](https://i.imgur.com/Jaty9Ly.jpg)

เครื่องมือนี้ของ mozilla ช่วยเราได้ [ssl-config-generator](https://mozilla.github.io/server-side-tls/ssl-config-generator/)

![Imgur](https://i.imgur.com/h94WhJs.png)
height="316" width="640"

เมื่อตรวจสอบ version ของ apache และ openssl ใน server แล้วเลือกตามเลขที่ได้มา configuration ก็จะแสดงในกล่องด้านล่าง

จากที่ลองเลือกเล่นๆ สังเกตได้ว่า ถ้าอยากได้ modern config จะต้องเป็น nginx หรือ apache version ใหม่ๆ ถ้าของใครใส่ config
เพิ่มแล้วเปลี่ยนจาก obsolete แล้วไม่ได้ modern ก็ไม่ต้องแปลกใจนะครับ

ของผมได้แบบนี้

```apache
<VirtualHost *:443>
    ...
    SSLEngine on
    SSLCertificateFile      /path/to/signed_certificate
    SSLCertificateChainFile /path/to/intermediate_certificate
    SSLCertificateKeyFile   /path/to/private/key
    SSLCACertificateFile    /path/to/all_ca_certs


    # HSTS (mod_headers is required) (15768000 seconds = 6 months)
    Header always set Strict-Transport-Security "max-age=15768000"
    ...
</VirtualHost>
```

**intermediate configuration, tweak to your needs**

```apache
SSLProtocol             all -SSLv2 -SSLv3 -TLSv1
SSLCipherSuite          ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK
SSLHonorCipherOrder     on
```
ตรง `/path/to/...` เนี่ยก็คือที่ที่เราเก็บ certificate files ของเราเอาไว้ ซึ่งถ้าอ่านมาจาก post ที่แล้วเรื่อง[การสร้างใบรับรองด้วย Let's Encrypt](http://dahoba.blogspot.com/2016/01/lets-encrypt-ssl-server-manual-certonly.html)

ดูว่า Header มี configuration ตามตัวอย่างหรือยัง ตรวจสอบด้วยว่าเปิดใช้งาน module `mod_headers` แล้ว

ส่วน `SSLCertificate*` เราจะไม่ต้องแก้ไข ให้ไปดูตรง SSLProtocol, SSLCipherSuite, SSLHonorCipherOrder ว่าตรงกัน หรือมีครบตามที่เครื่องมือบอกมาแล้วหรือยัง

แก้ไขแล้วเซฟไฟล์ รีสตาร์ท apache เพื่อให้มันอ่าน configuration ใหม่เข้าไป แค่นี้แหละเสร็จแล้ว :)