+++
author = "daho"
comments = true
date = "2016-08-18T11:16:19+07:00"
draft = true
image = ""
menu = ""
share = true
slug = "post-title"
tags = ["tag1", "tag2"]
title = "5 สิ่งที่ควรรู้เมื่อจะใช้ LINE Login API"

+++

เขียนแอพเล่นกับน้องๆที่ทำงาน จะให้แม่ค้าใช้ และอยากให้ผู้ใช้มีตัวตนในระบบของเราด้วย คิดไปคิดมา เราต้องใช้ สมาชิก LINE นี่แหละ และ LINE เค้าก็เปิด REST API ให้ใช้แล้วด้วย ต้องลองซะหน่อย 

จะ blog เกี่ยวกับ code Angular 2 app เรียกใช้ LINE API ด้วย แต่ขอแยกเป็นอีกโพสนะครับ รอติดตาม ;) ตอนนี้เอาเรื่องเรียกน้ำย่อยกันไปก่อน 

จะพัฒนา webapp ให้ user login ด้วย LINE ได้ เราต้องรู้ ต้องทำอะไรบ้าง 

**1 LINE Business Center** 

จะใช้งาน web services ของเขา เราก็ต้องไปสมัครสมาชิกก่อน ต้องไปสร้าง business account ที่ [LINE Business Center](https://business.line.me/) จะต้องใส่ข้อมูลบริษัทด้วย แล้วก็เลือกว่าเราจะใช้บริการอันไหนของ LINE ตอนนี้ก็มี 2 อันเท่าที่เห็น คือ Login API กับ BOT API

เมื่อสมัครส่วน business แล้วเราจะต้องสร้าง app ขึ้นมาตัวนึง ก็คือตัวที่จะใช้เชื่อมกับ บริการ login นี่แหละ

วิธีการสมัครไม่ยากเย็นนัก ทำตามขั้นตอนไปเรื่อยๆ โพสนี้ไม่ได้รีวิวให้อ่านกันนะ แปะลิ้งไว้ให้ตามไปอ่านกันก็แล้วกันนะ [Channel registration](https://developers.line.me/web-login/channel-registration)

**2 Web Login documentation**

ส่วนนี้จะเกี่ยวกับการพัฒนาแอพละ [LINE Developers](https://developers.line.me/channels/)
จากส่วนนี้เราจะมี/จะได้ channel id และ channel secret เอาไว้ใช้ในแอพของเรา 

เอกสารการเทคนิคการเชื่อมกับระบบ LINE Web Login ก็อยู่ที่ [Web Login](https://developers.line.me/web-login/integrating-web-login) 

ในขั้นตอนกรอกข้อมูลของแอพที่จะใช้บริการจะเจอสิ่งที่เรียกว่า `Authentication domain` 

**3 Authentication domain**

ในการใช้งาน Web Login เราจะต้องมี callback URL หรือ web server ที่จะเป็นตัวรับ authorization code จาก LINE server ด้วย 

ถ้างงว่า authorization code คืออะไรยังไง ตามไปอ่านเรื่อง [OAuth คืออะไร](http://www.dahoba.xyz/2016/08/oauth.html) กันได้ครับ

ข้อนี้เป็นสิ่งหนึ่งที่ทำให้อึ้งไปพักนึง ถ้าเคยเล่น Google Sign-in หรือ Facebook Sign-in จะรู้ว่าเราสามารถใช้ callback URL เป็น `http://localhost/callback` หรือเครื่องที่เราใช้พัฒนาแอพได้เลย แต่ LINE ไม่เป็นเช่นนั้น T_T 

เราจะต้องหา web server ที่ LINE server วิ่งมาหาแล้วเจอ (สามารถ access ได้จาก internet นั้นแล)

**4 Web Login ใช้กับ back-end web application เท่านั้น**

ผมอาจจะโง่ไปเอง ผมจะทำ responsive webapp  และแล้วเขียนเรียก API login ด้วย front-end code เลย มันทำไม่ได้ T_T ลองหลายท่าแล้ว แต่เหมือนกับว่า API เค้าทำมาให้สำหรับฝั่ง back-end เรียกใช้เท่านั้น 

ปัญหาที่เจอคือ LINE API ไม่ได้ทำรองรับ CORS คือการเรียก API ด้วย Javascript/Ajax ก็คือการเรียก API ระหว่าง domain ของเรากับของ LINE ทำไม่ได้เลย 

**5 Free hosting for back-end**

หากไม่มี web server ของตัวเอง ที่จะเอาไว้ host file ส่วน front-end ก็มีหลายทางเลือกนะครับ เช่น 

1. สมัคร AWS (Amazon Cloud) มี account ให้ทดลองใช้ 1 ปีเลย  
ถ้าใช้ AWS ก็ต้องติดตั้งเองทุกอย่าง อยากได้ back-end เป็นภาษาอะไรก็จัดการได้เอง
2. ใช้พวก free hosting แบบดั้งเดิม ที่เป็น apache/php/mysql ก็ได้  
ถ้าจะเขียน back-end เป็น php ผมชอบใช้ [Hostinger](http://api.hostinger.in.th/redir/1054306) เจ้า hostinger นี้เราจะลง Drupal, Wordpress ฯลฯ แบบใช้เอาจริงจัง หรือเอาไว้ศึกษาก็ได้ 
3. ถ้าอยากใช้ backend เป็น nodeJS หรือ Java แนะนำ [Openshift](https://www.openshift.com/)
ให้เรา deploy app ที่เป็น nodejs หรือ java webapp ได้ฟรี แบบจำกัดทรัพยากร cpu กับ ram น้อยหน่อย 
4. ใช้พวก free hosting สมัยใหม่ เค้าเรียกตัวเองว่า static hosting เอาไว้ host files ส่วนที่เป็น Front-end กัน เช่น [Netlify](https://www.netlify.com/), [Firebase](https://firebase.google.com/docs/hosting/)  

ส่วนตัวที่ใช้ ลอง Netlify ครับ เขาให้เรา 

* deploy code จาก git repository ได้ รองรับ github, bitbucket, gitlab ด้วย
* build project ที่ใช้ npm, gulp ฯให้เราได้ด้วย
* ให้ใช้ SSL(HTTPS) ได้และยังผูก domain name ของเราได้ด้วย ฟรี!

รายละเอียดการใช้งาน Netlify เอามาเขียนอีก blog ได้เลย รอติดตามกันได้
