+++
author = "+SiritasS"
comments = true
date = "2016-10-10T11:23:00+07:00"
draft = false
image = ""
menu = ""
share = true
    slug = "howto-osx-10.12-sierra-read-write-ntfs"
tags = ["osx", "tips"]
title = "วิธีทำให้ OSX 10.12 Sierra อ่าน/เขียน NTFS ได้"

+++

เมื่อ​ Apple ออก OS version ใหม่ เราก็ต้องมาหาวิธีทำให้มัน เขียนไฟล์ลงใน drive NTFS กันอีกครา 

ในโพสนี้เราก็จะมาทำให้ macOS Sierra มันเขียน drive ที่ format เป็น NTFS ซึ่งใช้กับ Window OS  เป็นหลัก วิธีที่เคยใช้กับ [Yosimite](http://www.dahoba.xyz/2014/10/howto-osx-10.10-yosemite-read-write-ntfs.html) หรือ [El Capitan](http://www.dahoba.xyz/2015/10/os-x-os-x-1011-el-capitan-ntfs.html) ใช้ไม่ได้กับ Sierra นะ <!--more-->

วิธีสำหรับ Sierra จะว่าง่ายก็ง่าย (ถ้าผู้อ่านเป็นนักพัฒนาซอฟต์แวร์ ก็คิดว่าไม่น่าจะยาก) แต่จะว่ายากก็ไม่เชิง ถ้าทำตามที่บอกเป๊ะๆ ก็ควรจะทำได้สำเร็จเช่นกัน ;)  

เกริ่นย่อหน้าสุดท้ายก่อนลุย วิธีทำให้เขียน drive NTFS นี้เป็น solution แบบฟรี อาจจะเขียนช้า (กล่าวคือ เวลาที่ใช้ในการ copy ไฟล์จาก Mac มาลง อาจจะช้ากว่า เวลาที่ใช้ Windows  copy ไฟล์มาใส่ drive เดียวกันนี้) หากท่านใดต้องการความเร็ว แนะนำให้ใช้ solution แบบเสียเงิน ลองดูของ [Tuxera NTFS for Mac](https://www.tuxera.com/products/tuxera-ntfs-for-mac/) หรือ [Paragon NTFS for Mac](https://www.paragon-software.com/home/ntfs-mac) ครับ

**TLDR;**  ถ้าเข้าใจตรงกันแล้ว ลุยกันเลย!

### ติดตั้ง [Homebrew](http://brew.sh/)

เปิด Terminal app ขึ้นมาก่อน 
พิมพ์คำสั่งนี้ หรือจะ copy ไปใส่แล้ว enter ก็ได้ เวลา copy คำสั่ง ไม่ต้องเอาตัว `$` ไปด้วยนะครับแสดงให้เห็นเฉยๆ ว่าเป็นคำสั่งใน Terminal

```sh
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

ควรจะเห็นหน้าจอแบบข้างล่างนี้  

เมื่อเห็น `Password:` ขึ้นมารอ เราจะต้องใส่ password ที่เราใช้ login เข้าเครื่องเราด้วย แล้วกด Enter

```sh
==> This script will install:
/usr/local/bin/brew
...
/usr/local/Homebrew
==> The following new directories will be created:
/usr/local/Cellar
/usr/local/Homebrew
...

Press RETURN to continue or any other key to abort
==> /usr/bin/sudo /bin/mkdir -p /usr/local/Cellar /usr/local/Homebrew /usr/local/Frameworks /usr/local/bin /usr/local/etc /usr/local/include /usr/local/lib /usr/local/opt /usr/local/sbin /usr/local/share /usr/local/share/zsh /usr/local/share/zsh/site-functions /usr/local/var
Password:
```

เมื่อใส่ password ถูกแล้ว กด  Enter แล้ว หน้าจอก็จะไปต่อ ประมาณนี้ 

ถ้าเครื่องที่เรารันคำสั่งยังไม่ได้ติดตั้ง xcode command line tools script นี้มันก็จะติดตั้งให้ด้วยเลย 

เราจะต้องใส่ password อีกครั้ง เมื่อหน้าจอแสดงบรรทัด 

>
>==> Installing Command Line Tools (macOS Sierra version 10.12) for Xcode-8.0
Copyright 2002-2015 Apple Inc.

>...

>Password:
>

```sh
...
==> Searching online for the Command Line Tools
==> /usr/bin/sudo /usr/bin/touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress
==> Installing Command Line Tools (macOS Sierra version 10.12) for Xcode-8.0
==> /usr/bin/sudo /usr/sbin/softwareupdate -i Command\ Line\ Tools\ (macOS\ Sierra\ version\ 10.12)\ for\ Xcode-8.0
Software Update Tool
Copyright 2002-2015 Apple Inc.
...
Password:
==> /usr/bin/sudo /usr/bin/xcode-select --switch /Library/Developer/CommandLineTools
```

เมื่อเห็น ``` Run  `brew help` to get started ``` 

```sh
==> Downloading and installing Homebrew...
...
==> Installation successful!
==> Next steps
Run `brew help` to get started
Further documentation: https://git.io/brew-docs
```

ให้พิมพ์คำสั่งนี้ต่อ เพื่อติดตั้ง Brew Cask

```sh
 $ brew tap caskroom/cask
```
เสร็จแล้ว 

### ติดตั้ง osxfuse

**ขั้นตอนนี้หากใครเคยทำ NTFS ใน Yosimite หรือ El Capitan แล้วน่าจะมี osxfuse ติดตั้งกันอยู่แล้ว อาจจะใช้การ upgrade ผ่านทาง GUI ก็ได้นะ ข้ามขั้นตอนไปติดตั้ง ntfs-3g version ใหม่

<img src="https://i.imgur.com/oi622ozl.png" title="source: imgur.com" />  
กด Update OSXFUSE ได้เลย 

<img src="https://i.imgur.com/Z2QUzell.png" title="source: imgur.com" />  
จะต้องใส่ password, เดียวกับตอน login
 
<img src="https://i.imgur.com/FgLVNakl.png" title="source: imgur.com" />   
เมื่อ update เสร็จ จะเห็นว่าเลขเวอร์ชันเป็น 3.5.2 ตามภาพ 

 
ส่วนหากมาแบบสดใหม่ ไม่เคยลง osxfuse มาก่อนให้ทำตามนี้ 
โดยพิมพ์คำสั่ง `brew cask install osxfuse` ก็ควรจะเห็นผลอย่างข้างล่าง

```sh
$ brew cask install osxfuse

==> Tapping caskroom/cask

...

You must reboot for the installation of osxfuse to take effect.

==> Downloading https://github.com/osxfuse/osxfuse/releases/download/osxfuse-3.5
######################################################################## 100.0%
==> Verifying checksum for Cask osxfuse
==> Running installer script /usr/sbin/installer
==> installer: Package name is FUSE for macOS
==> installer: choices changes file '/usr/local/Caskroom/osxfuse/3.5.2/Extras/Ch
==> installer: Installing at base path /
==> installer: The install was successful.
🍺  osxfuse was successfully installed!
```

### ติดตั้ง ntfs-3g version ใหม่

```sh
 $ brew install homebrew/fuse/ntfs-3g

==> Tapping homebrew/fuse

...

==> Summary
🍺  /usr/local/Cellar/gettext/0.19.8.1: 1,934 files, 16.9M
==> Installing homebrew/fuse/ntfs-3g
==> Using the sandbox
==> Downloading https://tuxera.com/opensource/ntfs-3g_ntfsprogs-2016.2.22.tgz
######################################################################## 100.0%
==> ./configure --prefix=/usr/local/Cellar/ntfs-3g/2016.2.22 --exec-prefix=/usr/
==> make
==> make install
🍺  /usr/local/Cellar/ntfs-3g/2016.2.22: 90 files, 1.7M, built in 2 minutes 13 seconds
```
รอสักครู่ใหญ่ๆ เมื่อเห็นตามภาพข้างบนแล้ว ลองเอา drive NTFS มาเสียบดู ก็น่าจะใช้ได้แล้ว ทดสอบง่ายอาจจจะลอง New Folder ที่ drive ดูก็ได้ **แต่** ช้าก่อน! หากปล่อยไว้เท่านี้ เวลาเรา reboot เครื่อง Mac เมื่อไหร่ มันจะกลับกลายเป็น Read only อีก 

####Enter recovery mode เพื่อทำให้ script รันอัตโนมัติ (reboot แล้วไม่หาย)

เคยเขียนเอาไว้แล้ว [วิธีเข้า recovery mode](http://www.dahoba.xyz/2015/11/os-x-recovery-mode.html)

เมื่อเข้า recovery mode ได้แล้ว 

* เปิด  Terminal app
* สั่ง `csrutil disable` เพื่อปิด SIP ก่อน
* สำรอง script `mount_ntfs` ที่ Apple มีมาให้ (script ที่ทำให้ drive NTFS เป็น mode อ่านอย่างเดียว หรือทั้งอ่านทั้งเขียนได้)
* `/Volumes/850evo` แต่ละเครื่องจะไม่เหมือนกันนะ ให้ดูของเครื่องตัวเองให้ดี ส่วนมากน่าจะเป็น `/Volumes/MacintoshHD`

```
 $ mv /Volumes/850evo/sbin/mount_ntfs /Volumes/850evo/sbin/mount_ntfs.orig
```
* สร้าง symbolic link ไปยัง script ที่ ntfs-3g ให้มา

```
 $ ln -s /Volumes/850evo/usr/local/Cellar/ntfs-3g/2016.2.22/sbin/mount_ntfs /Volumes/850evo/sbin/mount_ntfs
```
ก่อนจะ  restart เพื่อกลับเข้า mode ปกติ เราควรจะเปิด หรือ enable SIP กลับมาก่อน 
โดยใช้คำสั่ง ```$ csrutil enable```

แล้วก็ restart เพื่อกลับเข้า mode ปกติเรียบร้อย เท่านี้เอง Mac เราก็จะเขียน drive ที่เป็น NTFS ได้แล้ว :D 

  อ้างอิง [osxfuse's Wiki](https://github.com/osxfuse/osxfuse/wiki/NTFS-3G)
