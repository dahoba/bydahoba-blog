+++
date = "2016-01-12T16:58:21+07:00"
draft = false
title = "หัดใช้ Hugo เขียน blog"
image = ""
menu = ""
share = true
slug = "learn-to-blogging-with-hugo"
tags = ["general"]
+++
Hugo เป็น static site generator ที่เขียนด้วยภาษา Go ที่มันน่าสนใจเพราะ 

* มันเขียนด้วย Go ... LOL 
* มี themes ให้เลือกเยอะและสวยงาม
* เขียน content ด้วย markdown ได้ อันนี้ชอบสุดๆ 
* สร้าง static site มีแค่ http server ก็รันได้แล้ว ไม่ต้องติดตั้ง Go ที่ server เลย  

มาลองดูกันว่ามันง่าย และน่าใช้ขนาดไหน 
<!--more-->

การติดตั้ง ต้องมี homebrew ติดตั้งซะก่อน ดู [ติดตั้ง Homebrew](http://brew.sh/) 

ถ้าติดตั้งแล้ว เปิด teminal ใช้คำสั่งแบบนี้ เพื่อติดตั้ง Hugo

```sh
$ brew update && brew install hugo
```

หากเนื้อหาของบล๊อคที่จะเขียน อยากแสดง source code hilight เราจะต้องใช้ Pygments ซึ่งเขียนด้วยภาษา python ก็ต้องติดตั้ง python ด้วย ถ้าคุณใช้ mac เหมือนผม ก็ง่ายหน่อย พิมพ์คำสั่งนี้ที่ terminal เลย

```sh
$ sudo easy_install Pygments
```

###  สร้าง site 
สร้าง folder สำหรับ site ที่เราจะสร้างกันก่อน

```sh
$ mkdir hugo-site
$ cd hugo-site
```

ทีนี้เราจะให้ Hugo สร้างโครงสร้างของ site ที่เป็น hugo ให้เรา ด้วยการสั่งคำสั่ง `path/to/site` ก็คือ path ที่สร้างเตรียมเอาไว้ขั้นตอนที่แล้ว

```sh
$ hugo new site path/to/site
$ cd path/to/site
``` 
static site ที่ hugo สร้างให้ จะออกมาแบบนี้ 

* archetypes/
* content/
* data/
* layouts/
* static/
* config.toml

ตอนนี้จะยังไม่มี content หรือ configuration ใดๆ 

### ลองสร้าง content แรก
พิมพ์ใน terminal 

```sh
$ hugo new about.md
```

hugo  จะสร้างไฟล์เทมเพลตขึ้นมาให้ เนื้อหาแบบนี้ 

```
+++
date = "2016-01-12T16:58:21+07:00"
draft = true
title = "about"

+++
```
 เวลาเราจะเพิ่มเนื้อหา hugo จะรับ format แบบ markdown ให้เริ่มพิมพ์ต่อจาก `+++` ตัวล่าง  
 
 ไฟล์ `about.md` จะถูกสร้างขึ้นภายใต้ folder **content** ทดลองสร้างอีกไฟล์
 
 ```sh
 $ hugo new post/first.md
 ```
 
 ไฟล์ first.md จะถูกสร้างไว้ที่ **content/post/** 
 
ต่อไปเราต้องการ template และ theme เพื่อให้มันแสดงเนื้อหาของเราได้สวยงาม 
 
### ติดตั้ง themes

วิธีติดตั้ง theme pack พิมพ์ใน  terminal 

```sh
git clone --depth 1 --recursive https://github.com/spf13/hugoThemes.git themes
```
hugo มี theme ให้เลือกเยอะพอสมควร ลองเลือกๆกันดูระหว่างรอ github clone ลงมา

เมื่อ theme pack ถูก clone ลงมาเรียบร้อยแล้ว (จะสังเกตได้ว่ามันดาวโหลดมาอยู่ใต้ folder themes) เรามาลองทดสอบว่า site เราจะหน้าตาเป็นอย่างไร 

```sh 
$ hugo server --theme=hyde --buildDrafts
2 of 2 drafts rendered
0 future content
2 pages created
0 paginator pages created
0 tags created
0 categories created
in 14 ms
Watching for changes in /Users/example/workspace/hugo-site/{data,content,layouts,static,themes}
Serving pages from memory
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```
option 2 ตัวที่ใส่เข้าไปมีความหมายว่า

* `--theme` บอก hugo ว่าเราจะใช้ theme ไหน 
* `--buildDrafts` ให้เอาเนื้อหาที่ยังเป็น draft อยู่มาแสดงด้วย 

### แก้ไข content 
นอกจาก hugo สามารถรัน server ได้ด้วยตัวเอง และมันยังสามารถทำ live reload ให้เราได้ด้วย ขณะที่รัน server อยู่ ไฟล์เราก็แก้ไขไป save เมื่อไหร่ เนื้อหาใน site จะถูก update ตามทันที 

สังเกตได้ใน terminal ว่ามัน reload แล้ว

```sh 
Change detected, rebuilding site
2016-01-12 17:31 +0700
2 of 2 drafts rendered
0 future content
2 pages created
0 paginator pages created
0 tags created
0 categories created
in 10 ms
```

ถ้าสงสัยว่า option ของคำสั่ง hugo มีอะไรบ้างนอกจากจะไปดู documents เค้าแล้ว เราสามารถศึกษาได้จาก command line เช่นกัน 

```sh
$ hugo help
```
อันนี้ถ้าอยากรู้ว่า option ของการรัน server มีอะไรบ้าง 

```sh
$ hugo help server
```
