+++
date = "2016-01-12T16:57:19+07:00"
draft = false
title = "Licensed of products and services on this website."
image = "https://i.imgur.com/xjPtdpw.jpg"
tags = ["About"]
comments = false
+++

### Fonts

* [Google Fonts](https://fonts.google.com/) Athiti, Lato, Inconsolata
* [Fonts Awesome](http://fontawesome.io/icons/)

### Images, Photos

* [Pixabay](https://pixabay.com/)
* [Picjumbo](https://picjumbo.com/)

### Services

* Website สร้างด้วย [Hugo](https://gohugo.io/)
* Hosting ฟรีๆที่ [Bitbucket](https://bitbucket.org/)
