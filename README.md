# README

### Config

[themes.gohugo.io/casper](http://themes.gohugo.io/casper/)

[Hugo config](https://gohugo.io/overview/configuration/)

### Metadata on each file (Front matter)

```
+++
author = ""
date = "2014-07-11T10:54:24+02:00"
draft = false
title = "dotScale 2014 as a sketch"
slug = "dotscale-2014-as-a-sketch"
tags = ["event","dotScale","sketchnote"]
image = "images/2014/Jul/titledotscale.png"
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = ""           # set "main" to add this content to the main menu
+++

Contents here
```

### Create new content base on casper themes

```
hugo new -t casper post/my-post.md
```

### Using

**Live server**

`$ hugo server`


**Pre deploy**

`$ hugo`

